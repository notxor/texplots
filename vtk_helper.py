import os.path
import subprocess
import warnings

try:
    from paraview.simple import *
    _paraview_available_ = True
except:
    _paraview_available_ = False


class ErrorObserver:
    def __init__(self):
        self.__ErrorOccurred = False
        self.__ErrorMessage = None
        self.CallDataType = 'string0'

    def __call__(self, obj, event, message):
        self.__ErrorOccurred = True
        self.__ErrorMessage = message

    def ErrorOccurred(self):
        occ = self.__ErrorOccurred
        self.__ErrorOccurred = False
        return occ

    def ErrorMessage(self):
        return self.__ErrorMessage


class VtkState:
    def __init__(self, name: str=None, state: str=None):
        self.name = name
        self.state = state

    def generate(self, base_dir=''):
        assert self.state is not None
        pxm = servermanager.ProxyManager()
        pxm.UnRegisterProxies()
        del pxm
        Disconnect()
        Connect()
        e = ErrorObserver()
        renderView1 = GetActiveViewOrCreate('RenderView')
        renderView1.AddObserver('ErrorEvent', e)
        try:
            LoadState(self.state)
            Render()
            SaveScreenshot(self.name, ImageResolution=[1920, 1080], TransparentBackground=0)
        except:
            print('Error happended within paraview.')
            exit(-1)

        if e.ErrorOccurred():
            print('ERROR:', e.ErrorMessage())
            exit(-1)

        pxm = servermanager.ProxyManager()
        pxm.UnRegisterProxies()
        del pxm
        Disconnect()


# TODO allgemeiner
class Vtk:
    def __init__(self, name='', source=None, T=None, coloring='u', scheme='rainbow'):
        self.name = name
        self.source = source
        self.T = T

        self.coloring = coloring
        self.scheme = scheme

        self.solution = None
        self.renderView = None

    def set_camera(self):
        self.renderView.ResetCamera()

    def plot(self):
        solutionDisplay = Show(self.solution, self.renderView)
        solutionDisplay.Representation = 'Surface'
        ColorBy(solutionDisplay, ('POINTS', self.coloring))

    def create_dummy(self, filename):
        if not os.path.exists(filename):
            warnings.warn('Create dummy file {}'.format(filename), RuntimeWarning)
            subprocess.call(['convert', '-size', '32x32', 'xc:black', filename])
        else:
            warnings.warn('Could not update image {} but an older version exists.'.format(filename), RuntimeWarning)

    def generate_single(self, input_filename, output_filename):
        assert _paraview_available_

        Connect()

        # get active view
        self.renderView = GetActiveViewOrCreate('RenderView')
        self.renderView.ViewSize = [1920, 1080]
        self.renderView.Background = [1.0, 1.0, 1.0]
        self.renderView.OrientationAxesVisibility = 0
        self.renderView.ResetCamera()

        self.solution = self.load_solution(input_filename)
        self.set_default_colors()
        self.renderView.ResetCamera()
        self.plot()
        self.renderView.Update()
        self.set_camera()
        self.renderView.Update()

        # save screenshot
        if os.path.exists(output_filename):
            os.remove(output_filename)
        SaveScreenshot(output_filename, self.renderView, ImageResolution=[1920, 1080],
            TransparentBackground=0)

        self.Clear()

        return output_filename

    def load_solution(self, input_filename):
        # create a new 'XML Partitioned Unstructured Grid Reader'
        self.solution = XMLPartitionedUnstructuredGridReader(FileName=[input_filename])
        self.solution.PointArrayStatus = [
            'u', 'grad_u', 'grad_phi',
            'sigma', 'sigma_plus', 'sigma_minus', 'lambda',
            'phi',
            'bulk_plus', 'fracture_energy'
        ]

    def set_default_colors(self):
        # get color transfer function/color map for 'phi'
        phiLUT = GetColorTransferFunction('phi')
        phiLUT.RescaleTransferFunction(0.0, 1.0)
        phiLUT.ApplyPreset(self.scheme, True)

        uLUT = GetColorTransferFunction('u')
        uLUT.ApplyPreset(self.scheme, True)

    def Clear(self):
        pxm = servermanager.ProxyManager()
        pxm.UnRegisterProxies()
        del pxm
        Disconnect()

    def ResetSession(self):
        self.Clear()

    def generate(self, base_dir=''):
        if not _paraview_available_:
            warnings.warn('Paraview not available', RuntimeWarning)
            self.create_dummy(os.path.join(base_dir, self.name) + '.png')
            return

        if len(self.source) == 0:
            self.create_dummy(self.name + '.png')
            return

        files = []
        for desc in self.source:
            if len(self.source) == 1:
                t = None
            else:
                t = desc['t']

            out = self.name + '.png' if t is None else '{}_{}.png'.format(self.name, t)
            out = os.path.join(base_dir, out)
            file = self.generate_single(desc['filename'], out)
            files.append(file)

        return files


class Vtk2d(Vtk):
    def __init__(self, **kwargs):
        Vtk.__init__(self, **kwargs)

    def set_camera(self):
        self.renderView.ResetCamera()


class Vtk3d(Vtk):
    def __init__(self, name='', source=None, T=None):
        Vtk.__init__(self, name=name, source=source, T=T)

    def plot(self):
        solutionDisplay = Show(self.solution, self.renderView)
        solutionDisplay.Representation = 'Outline'
        ColorBy(solutionDisplay, ('POINTS', 'u'))

        isoVolume1 = IsoVolume(Input=self.solution)
        isoVolume1.InputScalars = ['POINTS', 'phi']
        isoVolume1.ThresholdRange = [0.0, 0.1]

        isoVolume1Display = Show(isoVolume1, self.renderView)
        isoVolume1Display.Representation = 'Surface'
        ColorBy(isoVolume1Display, ('POINTS', 'fracture_energy'))

    def set_camera(self):
        self.renderView.CameraPosition = [21.711696363235728, -17.608548966727298, 23.143537707756302]
        self.renderView.CameraFocalPoint = [5.0, 5.0, 5.0]
        self.renderView.CameraViewUp = [0.289292270737218, 0.7200316772829232, 0.6307649053338689]
        # self.renderView.CameraParallelScale = 8.660254037844387
