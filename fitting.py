import inspect
import re
import types
import warnings

try:
    from scipy.optimize import curve_fit
    _has_scipy = True
except ImportError:
    warnings.warn('Warning: scipy.optimize not available. Fitting will be disabled.', ImportWarning)
    _has_scipy = False


class Fit:
    """
    @param model: a function f(x, *params) with *params being fitted, such that y = f(x).
                  Examples: model=lambda x, a: a*x**2 yields a quadratic fitting curve.

    @param tex_str: a function s(*params), that returns a tex compatible (pgfplot) expression of the model.
                    If none is given, it tries to convert the model automatically, but may fail to do so.
                    Example: tex_str=lambda a: 'a*x^2'.format(*a)
    """
    def __init__(self, model=None, tex_str=None, legend=None):
        assert model is not None
        assert callable(model)

        self.model = model
        self.params = None
        self.tex_str = tex_str

        self.legend_function = legend

    def fit(self, xdata=None, ydata=None):
        assert xdata is not None
        assert ydata is not None

        if _has_scipy:
            self.params, _ = curve_fit(self.model, xdata=xdata, ydata=ydata)
        else:
            self.params = None

    def tex_expression(self):
        if self.params is None:
            return None

        if self.tex_str is not None:
            assert callable(self.tex_str)
            return self.tex_str(self.params)

        if self.tex_str is None:
            # Try to convert the model to a Latex string for pgfplot:
            if isinstance(self.model, types.LambdaType):
                funcString = str(inspect.getsourcelines(self.model)[0])
                funcString = funcString.strip("['\\n'],").split(":")[1]
                funcString = funcString.replace('**', '^')

                for i in range(len(self.params)):
                    char = chr(ord('a') + i)
                    funcString = re.sub(
                        '({char})'.format(char=char),
                        '{}'.format(self.params[i]),
                        funcString)

                return funcString

        raise Exception('Error: Could not convert model to string. Please provide the string manually.')

    def legend(self):
        if self.legend_function is not None:
            if callable(self.legend_function):
                return self.legend_function(self.params)
            if isinstance(self.legend_function, str):
                return self.legend_function
            raise Exception('Error: unknown legend format.')
        return None
