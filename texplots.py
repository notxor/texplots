#!/usr/bin/python3

# Script to create plots over multiple refinement runs.

import os.path

import queue
import threading
import collections.abc

import configuration

from tex import *
from generators import *
from data_sources import *


warnings.simplefilter('ignore', category=ResourceWarning)


class CompileTex:
    # Either provide a config object: tex-file gets generated, saved and compiled.
    # Or provide filename of tex (without extension): pdf gets compiled
    def __init__(self, tex: Tex = None, name=None, compiler='pdflatex', tmp_dir=None, pdf_dir=None, compile=True):
        assert name is not None

        self.tex = tex
        self.name = name

        self.compiler = compiler
        self.compile = compile

        self.pdf_dir = pdf_dir
        self.tmp_dir = tmp_dir
        if self.tmp_dir is None:
            if self.pdf_dir is not None:
                self.tmp_dir = self.pdf_dir + '/tmp/'

    def __str__(self):
        return 'CompileTex[{}]'.format(self.name)

    def texfile(self):
        s = self.name + '.tex'
        # Only append tmp if Tex object is given, not when a raw CompileTex class is used!
        if self.tex is not None and self.tmp_dir is not None:
            s = os.path.join(self.tmp_dir, s)
        return os.path.abspath(s)

    def pdffile(self):
        s = self.name + '.pdf'
        if self.pdf_dir is not None:
            s = os.path.join(self.pdf_dir, s)
        return os.path.abspath(s)

    def generate_raw_tex(self):
        assert self.name is not None

        if self.compile:
            assert self.tmp_dir is not None
            # Tex files need to be compiled and placed in the place they are
            target_pdf = compile_texfile(
                filename=self.name + '.tex',
                compiler=self.compiler,
                result_dir=self.pdf_dir,
            )
            if target_pdf is not None:
                configuration.Default.postprocess_pdf(target_pdf)

    def generate_tex_obj(self, base_dir):
        assert isinstance(self.tex, Tex)
        assert self.name is not None

        if self.tmp_dir != '':
            os.makedirs(self.tmp_dir, exist_ok=True)
        if self.pdf_dir != '':
            os.makedirs(self.pdf_dir, exist_ok=True)

        content = self.tex.generate(base_dir)

        if content == '' or content is None:
            return

        with open(self.texfile(), 'wt') as f:
            f.write(content)

        if self.compile:
            target_pdf = compile_texfile(
                filename=self.texfile(),
                base_dir=base_dir,
                result_dir=self.pdf_dir,
                compiler=self.compiler,
            )
            if target_pdf is not None:
                configuration.Default.postprocess_pdf(target_pdf)

    def generate(self, base_dir=''):
        # Tex file given:
        if self.tex is None:
            self.generate_raw_tex()
            return None

        # Tex object given:
        if self.tex is not None:
            self.generate_tex_obj(base_dir)
            return None

        assert False


def generate(C, compile=True, pdf_dir=None, tmp_dir=None):
    if C is None:
        return

    # CSV is generated at configuration time.
    if isinstance(C, CSV):
        return

    # Raw CompileTex
    if isinstance(C, CompileTex):
        C.compile = compile
        C.pdf_dir = pdf_dir
        C.tmp_dir = tmp_dir
        C.generate(tmp_dir)
        return

    # Plots: use C.generate to obtain Tex object
    if hasattr(C, 'generate'):
        tmp = C.generate()
        if isinstance(tmp, Tex):
            tex = CompileTex(tmp, name=C.name, compile=compile, tmp_dir=tmp_dir, pdf_dir=pdf_dir)
            tex.generate(tmp_dir)
        return

    # Tex object:
    if isinstance(C, Tex):
        generate(CompileTex(C, tmp_dir='tmp'), compile=compile, pdf_dir=pdf_dir, tmp_dir=tmp_dir)
        return

    assert False


def create_parallel(configs, n_threads=1, compile=True, pdf_dir=None, tmp_dir=None):
    if n_threads == 1:
        for C in configs:
            # print('({}/{}) {}'.format(
            #     configs.index(C) + 1,
            #     len(configs),
            #     C.name if hasattr(C, 'name') else 'No Tex',
            # ))
            generate(C, compile=compile, pdf_dir=pdf_dir, tmp_dir=tmp_dir)
        return

    def worker():
        while True:
            C = q.get()
            if C is None:
                break
            # print('({}/{}) {}'.format(
            #     configs.index(C) + 1,
            #     len(configs),
            #     C.name if hasattr(C, 'name') else 'No Tex',
            # ))
            try:
                generate(C, compile=compile, pdf_dir=pdf_dir, tmp_dir=tmp_dir)
            finally:
                q.task_done()

    q = queue.Queue()
    for C in configs:
        q.put(C)

    threads = []
    for i in range(min(n_threads, len(configs))):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)

    # block until all tasks are done
    q.join()

    # stop workers
    for i in range(n_threads):
        q.put(None)
    for t in threads:
        t.join()


def strip_extensions(files):
    to_remove = ['pdf', 'csv', 'tex', 'png']
    result = []
    for f in files:
        trimmed = False
        for r in to_remove:
            if f.endswith(r):
                result.append(f[:-len(r) - 1])
                trimmed = True
                break
        if not trimmed:
            result.append(f)
    return result


def regex_filter(C):
    if len(sys.argv) >= 2:
        patterns = sys.argv[1:]

        # C == Config
        if hasattr(C, 'name'):
            return regex_filter(C.name)

        if isinstance(C, CSV):
            return regex_filter(C.filename) or regex_filter('csv')

        if isinstance(C, str):
            return any(matches(C, patterns)) or any(matches(C, strip_extensions(patterns)))

        if isinstance(C, collections.abc.Iterable):
            return any([regex_filter(c) for c in C])

        raise Exception('Unknown type for argument C')
    else:
        return True
