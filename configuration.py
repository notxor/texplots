from tex import Tex
from pdf import pdfcrop


class IDefault:
    @staticmethod
    def new_tex():
        raise NotImplementedError()

    @staticmethod
    def postprocess_pdf(pdf: str):
        raise NotImplementedError()

    @staticmethod
    def postprocess_img(img: str):
        raise NotImplementedError()


# Define some defaults that every plot should have, mainly to guarantee uniform visualizations.
class _Default(IDefault):

    _axis_opts = [
        'thick',
        'cycle list name = exotic',
        'solid',
    ]

    group_style = 'horizontal sep = 5 mm'

    @staticmethod
    def new_tex():
        tex = Tex()
        tex.axis_opts.extend(_Default._axis_opts)
        return tex

    @staticmethod
    def postprocess_pdf(pdf: str):
        print('Created PDF:', pdf)
        pdfcrop(pdf, pdf)

    @staticmethod
    def postprocess_img(img: str):
        pass


Default = _Default
