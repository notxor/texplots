import os


def pdfcrop(input_pdf: str, output_pdf: str):
    cmd = 'pdfcrop {} {}'.format(input_pdf, output_pdf)
    r = os.popen(cmd)
    r.read()
    r.close()
