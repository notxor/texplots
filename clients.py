from abc import ABC, abstractmethod
from helpers import *

import warnings


class Client(ABC):
    def __init__(self):
        super().__init__()

    # Return the content of the given file_description.
    @abstractmethod
    def open(self, file_description):
        pass

    # List all folders in the given directory.
    @abstractmethod
    def list_folders(self, base_folder):
        pass

    # List all (valid) files in the given directory.
    # Returns a list of file_descriptions, suitable for a call to open.
    @abstractmethod
    def list_files(self, base_folder):
        pass

    # Return false, if run on localhost.
    def is_remote(self):
        return True

    # Provide additional information about a file.
    def informations(self, file_description):
        pass


class LocalHost(Client):
    def __init__(self, *args, **kwargs):
        super().__init__()

    def open(self, file_description):
        assert isinstance(file_description, str)
        return open(file_description, 'r')

    def list_folders(self, base_folder):
        return [os.path.join(base_folder, f) for f in os.listdir(base_folder) if os.path.isdir(os.path.join(base_folder, f))]

    def list_files(self, base_folder):
        return list_files(base_folder)

    def is_remote(self):
        return False

    def informations(self, file_description):
        return {'filename': file_description}


try:
    from paramiko import *
    _paramiko_available = True
except ImportError:
    _paramiko_available = False
    warnings.warn('Warning: paramiko not found. No SSH features available.', ImportWarning)

if _paramiko_available:
    import stat

    class SSH(Client):
        def __init__(self, hostname, user, port=22):
            super().__init__()
            self.hostname = hostname
            self.port = port
            self.user = user

            self.ssh = SSHClient()
            self.ssh.load_system_host_keys()
            config = os.getenv('HOME') + '/.ssh/config'
            if os.path.exists(config):
                self.ssh.load_system_host_keys(config)
            self.sftp = None
            self.connected = False

        def __del__(self):
            self.disconnect()

        def connect(self):
            self.ssh.connect(hostname=self.hostname, port=self.port, username=self.user)
            self.sftp = self.ssh.open_sftp()
            self.connected = True

        def disconnect(self):
            self.ssh.close()
            self.connected = False

        def open(self, file_description):
            if not self.connected:
                self.connect()
            return self.sftp.open(file_description)

        def list_folders(self, base_folder):
            if not self.connected:
                self.connect()

            result = []
            for bf in make_list(base_folder):
                if bf[0] == '~':
                    bf = '.' + bf[1:]
                bf = self.sftp.normalize(bf)

                try:
                    entries = self.sftp.listdir_attr(path=bf)
                except FileNotFoundError:
                    entries = []
                    print('Warning: path', bf, 'not found')

                for e in entries:
                    path = os.path.join(bf, e.filename)
                    if stat.S_ISDIR(e.st_mode):
                        result.append(path)
            return result

        def list_files(self, base_folder, recursive=True, ignore_folders=None):
            if not self.connected:
                self.connect()

            result = []
            for bf in make_list(base_folder):
                if bf[0] == '~':
                    bf = '.' + bf[1:]
                bf = self.sftp.normalize(bf)

                try:
                    entries = self.sftp.listdir_attr(path=bf)
                except FileNotFoundError:
                    entries = []
                    print('Warning: path', bf, 'not found')

                for e in entries:
                    path = os.path.join(bf, e.filename)
                    if stat.S_ISDIR(e.st_mode):
                        if ignore_folders is not None and os.path.basename(path) in ignore_folders:
                            continue
                        if recursive:
                            result.extend(self.list_files(path))
                    else:
                        result.append(path)
            return result

        def informations(self, file_description):
            pass


if not _paramiko_available:
    class SSH(Client):
        def __init__(self, hostname, user, port=122):
            raise ConnectionError

        def open(self, file_description):
            raise ConnectionError

        def list_folders(self, base_folder):
            raise ConnectionError

        def list_files(self, base_folder):
            raise ConnectionError

        def is_remote(self):
            raise ConnectionError

        def informations(self, file_description):
            raise ConnectionError
