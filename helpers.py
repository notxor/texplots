import os.path
import re
import glob
from bisect import bisect_left
import csv
import warnings
import json
import numpy as np


def __DecimalToAnyBaseArrayRecur__(array, decimal, base):
    array.append(decimal % base)
    div = decimal // base
    if div == 0:
        return
    __DecimalToAnyBaseArrayRecur__(array, div, base)


def DecimalToAnyBaseArray(decimal, base):
    array = []
    __DecimalToAnyBaseArrayRecur__(array, decimal, base)
    return array[::-1]


def save_obj(obj, filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

    with open(filename, 'w') as f:
        json.dump(obj, f)


def load_obj(name):
    with open(name, 'rb') as f:
        return json.load(f)


def suffix(i):
    repr = DecimalToAnyBaseArray(i, 26)
    repr = [chr(ord('A') + r) for r in repr]
    return ''.join(repr)


def path(s):
    m = re.findall(r'.+(?=run)', s)
    if len(m) == 1:
        return m[0]
    else:
        return 0


def make_list(arg):
    if isinstance(arg, list) or isinstance(arg, tuple) or isinstance(arg, set) or isinstance(arg, dict):
        return arg
    else:
        return [arg]


def fix_expression(s):
    assert isinstance(s, str)
    if '=' not in s:
        return '=' + s

    if 'expr' in s:
        return ' ' + s

    return s


def sort_tables(tables):
    def run_nr(s):
        m = re.findall(r'(?<=run_)\d+', s)
        if len(m) == 1:
            return int(m[0])
        else:
            return 0

    def core_nr(s):
        m = re.findall(r'(?<=\d_)\d+', s)
        if len(m) == 1:
            return int(m[0])
        else:
            return 0

    def key(s):
        return path(s), run_nr(s), core_nr(s)

    tables.sort(key=key)
    return tables


# Returns files without those containing stuff like lock, ~, etc.
def regular_files(files):
    files = [f for f in files if '~lock' not in f]
    return files


# Get a list of all files in base_folder.
# Also searches subdirectories.
def list_files(base_folder):
    result = []
    for bf in make_list(base_folder):
        for F in glob.glob(bf):
            for root, dirs, files in os.walk(F):
                for f in files:
                    result.append(os.path.abspath(os.path.join(root, f)))
    return result


def find_subfolders(base_folder, regex='run_'):
    if isinstance(base_folder, (list, tuple, set)):
        result = []
        for bf in base_folder:
            tmp = find_subfolders(bf, regex)
            sort_tables(tmp)
            result.extend(tmp)
        return result
    else:
        result = [os.path.join(base_folder, o) for o in os.listdir(base_folder) if re.match(regex, o) is not None]
        return sort_tables(result)


# Find all files matching the given pattern.
def find_files(base_folders, pattern, rel_path=True):
    result = [f for f in list_files(base_folders) if re.search(pattern, f)]

    if rel_path:
        current_dir = os.path.abspath(os.getcwd())
        result = [os.path.relpath(path=f, start=current_dir) for f in result]

    return sort_tables(result)


# Load the given csv-file.
# Returns a dict with the column names as keys and the corresponding entries as values.
def read_csv(filename, csv_file=None):
    created = False
    if csv_file is None:
        created = True
        csv_file = open(filename, 'r')

    result = None
    csv_reader = csv.reader(csv_file, delimiter=' ', quotechar='"')
    header = None
    for row in csv_reader:
        # Header
        if not header:
            # Last entry may be empty, remove it
            if row[-1] == '':
                header = row[:-1]
            else:
                header = row

            # Try to detect if no header is given
            if not all([isinstance(convert(h), str) for h in header]):
                print('Warning: no header detected')
                if len(header) == 2:
                    result = {'key': [convert(header[0])], 'value': [header[1]]}
                else:
                    result = {'column_{}'.format(i): [convert(h)] for i, h in enumerate(header)}
                header = list(result.keys())
            else:
                # All fine
                result = {c: [] for c in header}
        else:
            for i, k in enumerate(header):
                value = convert(row[i])
                # If entry is like {value}, remove braces.
                if isinstance(value, str):
                    if value.startswith('{') and value.endswith('}'):
                        value = value[1:-1]
                    if value.startswith('"') and value.endswith('"'):
                        value = value[1:-1]

                result[k].append(value)

    if created:
        csv_file.close()

    return result


# def write_csv(csv_data, filename):
#     dir = os.path.dirname(filename)
#     if dir != '':
#         os.makedirs(dir, exist_ok=True)
#     with open(filename, 'w') as csvfile:
#         min_size = 132456798
#         if len(list(csv_data.keys())) == 0:
#             return
#         for key in list(csv_data.keys()):
#             csvfile.write('{} '.format(key))
#             min_size = min(min_size, len(csv_data[key]))
#
#         for i in range(min_size):
#             csvfile.write('\n')
#             for key in (list(csv_data.keys())):
#                 value = convert(csv_data[key][i])
#                 if isinstance(value, int):
#                     csvfile.write('{} '.format(value))
#                     continue
#                 if isinstance(value, float):
#                     csvfile.write('{:.8e} '.format(value))
#                     continue
#                 if isinstance(value, str):
#                     # Pgfplots requires spaced strings to be embraced
#                     value = '{' + value + '}'
#                     csvfile.write('{} '.format(value))
#                     continue
#                 assert False
#
#         csvfile.close()


# Search the csv (dict()[]) for rows fulfilling the given conditions.
# Conditions are given as dict(), where values are either functions return true/false,
# or values used for comparison.
# The key denotes the column name, where the condition is evaluated.
# Multiple conditions are applied in an "and" manner.
def find_rows(csv, conditions):
    n_rows = 0
    for val in csv.values():
        n_rows = len(val)
        break

    result = list(range(n_rows))
    for col, condition in conditions.items():
        if col is not None and condition is not None:
            # Treat pgf expressions
            if 'expr' in col:
                data = parse_pgf_expression(col, csv=csv)
            else:
                data = csv[col]

            if callable(condition):
                result = [i for i in result if condition(data[i])]
            else:
                result = [i for i in result if data[i] == condition]

    return result


# Extract the given columns from the csv, where run equals a given value.
# def where_run_is(csv, colnames, run):
#     rows = find_rows(csv, {'run': run})
#     # Single column selected
#     if isinstance(colnames, str):
#         result = [csv[colnames][i] for i in rows]
#     else:
#         result = dict()
#         for colname in colnames:
#             result[colname] = [csv[colname][i] for i in rows]
#     return result

try:
    from fastnumbers import fast_real


    def convert(x):
        return fast_real(x)
except:
    warnings.warn('Warning: fastnumbers not available but required for fast loading.', RuntimeWarning)


    def _convert(s):
        try:
            v = int(s)
            return v
        except ValueError:
            pass

        try:
            v = float(s)
            return v
        except ValueError:
            pass

        return s


    convert = _convert


def sort_by(csv, sort_keys):
    if len(sort_keys) == 0:
        return

    sort_values = [csv[sort_key] for sort_key in sort_keys]
    L = [len(col_data) for col_data in sort_values]
    assert all_same(L)
    L = L[0]
    reordering = list(range(L))

    key_lambda = lambda i: \
        tuple(
            [sort_values[k][i] for k in range(len(sort_keys))])

    reordering.sort(key=key_lambda)

    for key in csv.keys():
        old = csv[key]
        csv[key] = [old[i] for i in reordering]


def matches(ss, expressions):
    return [len(re.findall(e, s)) > 0 for e in make_list(expressions) for s in make_list(ss)]


def any_matches(ss, expressions):
    return any(matches(ss, expressions))


def matches_dict(input_dict, ignore_dict):
    result = []
    for key, expr in ignore_dict.items():
        result.extend(matches(input_dict[key], expr))
    return result


def extension(path, ext):
    tmp = os.path.splitext(path)[-1].lower()
    if tmp == '.' + ext:
        return path
    else:
        return path + '.' + ext


def all_same(L):
    return all(x == L[0] for x in L)


def flatten(L):
    return [r for item in L for r in item]


# Remove duplicate rows from csv
def unique_rows(csv):
    keys = csv.keys()
    matrix = list(csv.values())
    matrix = [list(i) for i in zip(*matrix)]  # Transpose
    matrix = [list(tline) for tline in set(tuple(line) for line in matrix)]
    matrix = [list(i) for i in zip(*matrix)]  # Transpose
    # Build csv
    return {k: line for k, line in zip(keys, matrix)}


def summary(tables, line):
    tables = make_list(tables)
    assert len(tables) > 0

    # Make sure all entries have same last line (-1)
    if line == -1:
        n_lines = []
        for i, t in enumerate(tables):
            tmp = read_csv(tables[i])
            n_lines.append(len(list(tmp.values())[0]))

        assert min(n_lines) == max(n_lines)

    i = 0
    for f in tables:
        if i == 0:
            result = read_csv(f)

            for key in result.keys():
                result[key] = [result[key][line]]
        else:
            data = read_csv(f)

            for col_name in result.keys():
                if col_name in data.keys():
                    result[col_name].append(data[col_name][line])
                else:
                    result[col_name].append(0)

        i = i + 1

    # add source index (path) to data
    paths = [os.path.dirname(t) for t in tables]
    result['source'] = []
    for t in tables:
        result['source'].append(paths.index(os.path.dirname(t)))

    sort_keys = {'n_ranks', 'run'}
    sort_by(result, sort_keys & set(result.keys()))

    return result


def parse_pgf_expression(y, csv):
    # Remove 'expr ='
    expr = y.replace('expr', '')
    expr = expr.strip()
    assert expr[0] == '='
    expr = expr[1:]

    # Collect fields occuring in y and put data in numpy arrays
    regex = r'\\thisrow\{([^}]+)\}'
    fields = re.findall(pattern=regex, string=expr)
    data = {f: np.array(csv[f], dtype=float) for f in fields}

    # Generate numpy expression
    expr = re.sub(pattern=regex, string=expr, repl=r"data['\1']")
    # Evaluate
    values = eval(expr)
    return values


# Compute the values given by the pgf expression:
# Example:
# 1) expression='dofs': returns the column data['dofs']
# 2) expression=r'expr=\thisrow{dofs} / \thisrow{time}': returns data['dofs']/data['time'] (element-wise)
def get_values(expression: str, data):
    if 'expr' in expression:
        return parse_pgf_expression(expression, csv=data)
    else:
        return data[expression]


def strictly_increasing(L):
    return all(x < y for x, y in zip(L, L[1:]))


def strictly_decreasing(L):
    return all(x > y for x, y in zip(L, L[1:]))


def take_closest(myList, myNumber, lo=0):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber, lo=lo)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
        return after
    else:
        return before


# For each entry in my_numbers, returns the closest entry in myList
def take_closest_list(myList, my_numbers):
    # Assumes my_numbers is sorted!
    result = []
    pos = 0
    for myNumber in my_numbers:
        pos = bisect_left(myList, myNumber, lo=pos)
        if pos == 0:
            result.append(myList[0])
            continue
        if pos == len(myList):
            result.append(myList[-1])
            continue

        before = myList[pos - 1]
        after = myList[pos]
        if after - myNumber < myNumber - before:
            result.append(after)
            continue
        else:
            result.append(before)
            continue

    return result


try:
    import numpy as np
except ImportError:
    warnings.warn('Warning: numpy not available but required for downsampling.', ImportWarning)


    def sample_function(func, points, tol=0.05, min_points=16, max_level=16,
                        sample_transform=None):
        return points, func(points)
else:

    # License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0

    def sample_function(func, points, tol=0.05, min_points=16, max_level=16,
                        sample_transform=None):
        """
        Sample a 1D function to given tolerance by adaptive subdivision.

        The result of sampling is a set of points that, if plotted,
        produces a smooth curve with also sharp features of the function
        resolved.

        Parameters
        ----------
        func : callable
            Function func(x) of a single argument. It is assumed to be vectorized.
        points : array-like, 1D
            Initial points to sample, sorted in ascending order.
            These will determine also the bounds of sampling.
        tol : float, optional
            Tolerance to sample to. The condition is roughly that the total
            length of the curve on the (x, y) plane is computed up to this
            tolerance.
        min_point : int, optional
            Minimum number of points to sample.
        max_level : int, optional
            Maximum subdivision depth.
        sample_transform : callable, optional
            Function w = g(x, y). The x-samples are generated so that w
            is sampled.

        Returns
        -------
        x : ndarray
            X-coordinates
        y : ndarray
            Corresponding values of func(x)

        Notes
        -----
        This routine is useful in computing functions that are expensive
        to compute, and have sharp features --- it makes more sense to
        adaptively dedicate more sampling points for the sharp features
        than the smooth parts.

        Examples
        --------
        >>> def func(x):
        ...     '''Function with a sharp peak on a smooth background'''
        ...     a = 0.001
        ...     return x + a**2/(a**2 + x**2)
        ...
        >>> x, y = sample_function(func, [-1, 1], tol=1e-3)

        >>> import matplotlib.pyplot as plt
        >>> xx = np.linspace(-1, 1, 12000)
        >>> plt.plot(xx, func(xx), '-', x, y[0], '.')
        >>> plt.show()

        """
        return _sample_function(func, points, values=None, mask=None, depth=0,
                                tol=tol, min_points=min_points, max_level=max_level,
                                sample_transform=sample_transform)


    def _sample_function(func, points, values=None, mask=None, tol=0.05,
                         depth=0, min_points=16, max_level=16,
                         sample_transform=None):
        points = np.asarray(points)

        if values is None:
            values = np.atleast_2d(func(points))

        if mask is None:
            mask = Ellipsis

        if depth > max_level:
            # recursion limit
            return points, values

        x_a = points[..., :-1][mask]
        x_b = points[..., 1:][mask]

        x_c = .5 * (x_a + x_b)
        y_c = np.atleast_2d(func(x_c))

        x_2 = np.r_[points, x_c]
        y_2 = np.r_['-1', values, y_c]
        j = np.argsort(x_2)

        x_2 = x_2[..., j]
        y_2 = y_2[..., j]

        # -- Determine the intervals at which refinement is necessary

        if len(x_2) < min_points:
            mask = np.ones([len(x_2) - 1], dtype=bool)
        else:
            # represent the data as a path in N dimensions (scaled to unit box)
            if sample_transform is not None:
                y_2_val = sample_transform(x_2, y_2)
            else:
                y_2_val = y_2

            p = np.r_['0',
                      x_2[None, :],
                      y_2_val.real.reshape(-1, y_2_val.shape[-1]),
                      y_2_val.imag.reshape(-1, y_2_val.shape[-1])
            ]

            sz = (p.shape[0] - 1) // 2

            xscale = x_2.ptp(axis=-1)
            yscale = abs(y_2_val.ptp(axis=-1)).ravel()

            p[0] /= xscale

            p[1:sz + 1] /= yscale[:, None]
            p[sz + 1:] /= yscale[:, None]

            # compute the length of each line segment in the path
            dp = np.diff(p, axis=-1)
            s = np.sqrt((dp ** 2).sum(axis=0))
            s_tot = s.sum()

            # compute the angle between consecutive line segments
            dp /= s
            dcos = np.arccos(np.clip((dp[:, 1:] * dp[:, :-1]).sum(axis=0), -1, 1))

            # determine where to subdivide: the condition is roughly that
            # the total length of the path (in the scaled data) is computed
            # to accuracy `tol`
            dp_piece = dcos * .5 * (s[1:] + s[:-1])
            mask = (dp_piece > tol * s_tot)

            mask = np.r_[mask, False]
            mask[1:] |= mask[:-1].copy()

        # -- Refine, if necessary

        if mask.any():
            return _sample_function(func, x_2, y_2, mask, tol=tol, depth=depth + 1,
                                    min_points=min_points, max_level=max_level,
                                    sample_transform=sample_transform)
        else:
            return x_2, y_2
