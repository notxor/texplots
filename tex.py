from helpers import *

import warnings
import subprocess


class Tex:
    def __init__(self):
        self.level_macro = [
            '\\newcommand{\\LVL}[1]{',
            '\t\\pgfplotstablegetelem{{1}}{lvl}\\of{#1}',
            '\t\\pgfplotsretval',
            '}'
        ]

        self.packages = [
            # r'\RequirePackage{luatex85}',
            r'\documentclass[crop]{standalone}',
            '',
            r'\usepackage{tikz}',
            r'\usepackage{pgfplots}',
            r'\pgfplotsset{compat=newest}',
            r'\usepackage{pgfplotstable}',
            ''
        ]

        self.discard = [
            r'\pgfplotsset{',
            '\t' r'discard if not/.style 2 args={',
            '\t' '\t' r'x filter/.append code={',
            '\t' '\t' '\t' r'\edef\tempa{\thisrow{#1}}',
            '\t' '\t' '\t' r'\edef\tempb{#2}',
            '\t' '\t' '\t' r'\ifx\tempa\tempb',
            '\t' '\t' '\t' r'\else',
            '\t' '\t' '\t' '\t' r'\edef\tempa{\thisrow{#1}}',
            '\t' '\t' '\t' '\t' r'\edef\tempb{"#2"}',
            '\t' '\t' '\t' '\t' r'\ifx\tempa\tempb',
            '\t' '\t' '\t' '\t' r'\else',
            '\t' '\t' '\t' '\t' '\t' r'\def\pgfmathresult{inf}',
            '\t' '\t' '\t' '\t' r'\fi',
            '\t' '\t' '\t' r'\fi',
            '\t' '\t' r'}',
            '\t' r'}',
            r'}'
            '\n'
        ]

        self.lines = []
        self.sections = []
        self.tables = []
        self.axis_opts = []
        self.plots = []
        self.legends = []
        self.groups = []
        self.plot_count = 0

    def tabs(self):
        return '\t' * (len(self.sections))

    def newline(self):
        self.lines.append('')

    def add_discard(self):
        self.lines.extend([self.tabs() + line for line in self.discard])

    def add_tables(self, tables):
        if tables is not None:
            self.tables.extend([os.path.normpath(t) for t in make_list(tables)])

    def begin(self, section, options=None, omit=False):
        if options is None or len(options) == 0:
            if omit:
                self.lines.append(r'{tabs}\{section}'.format(tabs=self.tabs(), section=section))
            else:
                self.lines.append(r'{tabs}\begin{{{section}}}'.format(tabs=self.tabs(), section=section))
        else:
            if omit:
                self.lines.append('{tabs}\\{section}[\n\t{tabs}{options}\n{tabs}]'.format(
                    tabs=self.tabs(), section=section, options=',\n\t{}'.format(self.tabs()).join(options)))
            else:
                self.lines.append('{tabs}\\begin{{{section}}}[\n\t{tabs}{options}\n{tabs}]'.format(
                    tabs=self.tabs(), section=section, options=',\n\t{}'.format(self.tabs()).join(options)))

        self.sections.append((section, omit))

    def end(self):
        section = self.sections[-1]
        self.sections.pop()
        if not section[1]:
            self.lines.append(r'{tabs}\end{{{section}}}'.format(tabs=self.tabs(), section=section[0]))

    def end_all(self):
        while len(self.sections) > 0:
            self.end()

    def set_axis_opts(self, axis_opts):
        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)

    def add_group(self, tex):
        assert isinstance(tex, Tex)
        self.groups.append(tex)

    def add_parametric_plot(self, x: str, y: str, node: str = '', options: list = None,
                            append: bool = True, legend: str = None):
        """
        Add a parametric (x,y) plot without external data.
        :param x:       x-component
        :param y:       y-component
        :param node:    node name (pgfplots)
        :param options: plot options (pgfplots)
        :param append:  overwrite or append options
        :param legend:  legend description
        """
        assert x is not None
        assert y is not None

        s = r'{tabs}\addplot{append}[{options}] ( {{ {x} }} , {{ {y} }} ) {node};'.format(
            append='+' if append else '',
            x=x, y=y,
            tabs=self.tabs(),
            options=', '.join(options),
            node=node,
        )

        self.plots.append(s)
        self.plot_count += 1
        if legend is not None:
            self.legends.append(legend)

    # TODO split into plot_table, plot_function, plot_parametric
    def add_plot(self, x=None, y=None, table=None, options=None, legend=None, style=None, raw=None):
        if raw is not None:
            assert x is None
            assert y is None
            assert table is None
            assert options is None
            assert style is None
            self.plots.append(raw)
            self.plot_count += 1
            if legend is not None:
                self.legends.append(legend)
            return

        if options is None:
            options = []

        if table:
            table = os.path.normpath(table)
            if table in self.tables:
                i = self.tables.index(table)
                table = '\\table{}'.format(suffix(i))

            if style is not None:
                shift = style - self.plot_count
                self.plots.append(r'\pgfplotsset{{cycle list shift={}}}'.format(shift))

            if x is None and y is not None:
                xy = '[y {y}]'.format(y=fix_expression(y))
            else:
                if x is not None and y is not None:
                    xy = '[x {x}, y {y}]'.format(x=fix_expression(x), y=fix_expression(y))
                else:
                    xy = ''

            s = r'{tabs}\addplot+[{options}] table {xy} {{{table}}};'.format(
                xy=xy, tabs=self.tabs(), options=', '.join(options), table=table)
            self.plots.append(s)
            self.plot_count += 1

            if 'forget plot' in options:
                pass
                # self.legends.append('')
            else:
                self.legends.append(legend)

        if not table:
            assert y
            # assert not x

            # if 'forget' not in options:
            if legend is not None:
                self.legends.append(legend)
            else:
                options.append('forget plot')

            s = r'{tabs}\addplot+[{options}] {{{expr}}};'.format(
                tabs=self.tabs(), options=', '.join(options), expr=y)
            self.plots.append(s)

    def add_legends(self, legends=None):
        legends = legends or self.legends

        if any(legends):
            legends = [line if line else '' for line in legends]
            s = '\\\\'.join(legends)
            s += '\\\\'

            for i, table in enumerate(self.tables):
                s = s.replace(table, '\\table{}'.format(suffix(i)))

            self.newline()
            self.lines.append(r'{tabs}\legend{{{legend}}}'.format(tabs=self.tabs(), legend=s))

    # Convert tableA, tableB, ... into paths.
    def make_absolute_paths(self):
        for i, path in enumerate(self.tables):
            macro = '\\table{}'.format(suffix(i))
            self.plots = [p.replace(macro, path) for p in self.plots]
            self.lines = [p.replace(macro, path) for p in self.lines]

    # Convert paths to tableA, ...
    def make_macros(self, s):
        for i, path in enumerate(self.tables):
            macro = '\\table{}'.format(suffix(i))
            s = s.replace(path, macro)
        return s

    # Merge with another Tex object.
    def add(self, a):
        assert isinstance(a, Tex)
        a.make_absolute_paths()
        self.tables.extend(a.tables)
        self.plots.extend(a.plots)
        self.legends.extend(a.legends)

    def generate_groups(self, base_path):
        assert len(self.groups) > 0
        assert len(self.plots) == 0
        assert len(self.tables) == 0

        self.lines = []
        self.lines.extend(self.packages)
        self.lines.append(r'\usepgfplotslibrary{groupplots}')
        self.lines.append('')

        self.begin('document')

        # Revert to absolute paths to avoid confusion between groups
        for T in self.groups:
            T.make_absolute_paths()

        # Tables
        for T in self.groups:
            self.tables.extend(T.tables)

        for i, t in enumerate(self.tables):
            self.lines.append(
                '{}\\newcommand{{\\table{}}}{{{}}}'.format(self.tabs(), suffix(i), os.path.relpath(t, base_path)))
        self.newline()

        # Discard macro
        if any(['discard' in p for T in self.groups for p in T.plots]):
            self.add_discard()
            self.newline()

        # Level macro
        if any([L is None or 'LVL' in L for T in self.groups for L in T.legends]):
            self.lines.extend([self.tabs() + s for s in self.level_macro])
            self.newline()

        self.begin('tikzpicture')
        self.begin('groupplot', self.axis_opts)

        # Plots
        for g, T in enumerate(self.groups):
            if g > 0:
                self.lines.append('')
            self.begin('nextgroupplot', options=T.axis_opts, omit=True)
            self.lines.extend([self.tabs() + self.make_macros(s) + '\n' for s in T.plots])
            self.add_legends(T.legends)
            self.end()

        self.end_all()

        return '\n'.join(self.lines) + '\n'

    def generate(self, base_path=''):
        if len(self.groups) > 0:
            return self.generate_groups(base_path)

        self.lines = []
        self.lines.extend(self.packages)

        self.begin('document')

        # Tables
        for i, t in enumerate(self.tables):
            self.lines.append('{}\\newcommand{{\\table{}}}{{{}}}'
                              .format(self.tabs(), suffix(i), os.path.relpath(t, base_path)))
        self.newline()

        # Discard macro
        if any(['discard' in p for p in self.plots]):
            self.add_discard()
            self.newline()

        # Level macro
        if any([L is None or 'LVL' in L for L in make_list(self.legends)]):
            self.lines.extend([self.tabs() + s for s in self.level_macro])
            self.newline()

        self.begin('tikzpicture')
        self.begin('axis', self.axis_opts)

        # Plots
        if len(self.plots) == 0:
            warnings.warn('Nothing to be plotted. Maybe some data is missing?', RuntimeWarning)
        self.lines.extend([self.tabs() + self.make_macros(s) + '\n' for s in self.plots])
        self.add_legends()

        self.end_all()

        return '\n'.join(self.lines) + '\n'


def filter_tex_errors(output):
    # No fatal error, dont filter output.
    if len(re.findall('Fatal error ', output, re.MULTILINE)) == 0:
        return None

    text = output

    # Remove (/usr/.../file.sty) things.
    old = None
    while old != text:
        old = text
        text = re.sub(r'\([^()!]+\)', '', text, re.MULTILINE)

    # Remove some other stuff
    text = re.sub(r'.*This is .*TeX.*', '', text)
    text = re.sub(r'.*Package pgfplots notification.*', '', text)
    text = re.sub(r'.*restricted system commands enabled.*', '', text)
    text = re.sub(r'.*Transcript written.*', '', text)
    text = re.sub(r'.*LaTeX2e.*', '', text)
    text = re.sub(r'.*Babel.*', '', text)
    text = re.sub(r'.*EveryShipout initializing macros.*', '', text)
    text = re.sub(r'.*generated with the most recent feature.*', '', text)
    text = re.sub(r'.*See the pgfplots package documentation.*', '', text)
    text = re.sub(r'.*Type  H <return>  for immediate help.*', '', text)
    text = re.sub(r'.*words of node memory still in use.*', '', text)
    text = re.sub(r'.*if_stack.*', '', text)
    text = re.sub(r'.*hlist.*', '', text)
    text = re.sub(r'.*glue_spec.*', '', text)
    text = re.sub(r'.*pdf_literal.*', '', text)
    text = re.sub(r'.*pdf_colorstack.*', '', text)
    text = re.sub(r'.*avail lists.*', '', text)

    # Remove empty lines.
    split = text.split(sep='\n')
    split = [s for s in split if len(re.sub(r'\s+', '', s)) > 0]

    return split


# filename      The tex file to compile
# compiler      Compiler command
# result_dir    Where the pdf will be placed. Default: base_dir.
# base_dir      Where the command will be run. Important for relative paths in the texfile. Default: path of filename.
def compile_texfile(filename, result_dir=None, base_dir=None, compiler='pdflatex'):
    # Creates the pdf and auxiliary files in base_dir.
    base_dir = base_dir or os.path.dirname(filename)
    result_dir = result_dir or base_dir

    base_dir = os.path.abspath(base_dir)
    result_dir = os.path.abspath(result_dir)

    cmd = 'cd {base_dir} && max_print_line=10000 {compiler} ' \
          '-synctex=1 ' \
          '-interaction=nonstopmode ' \
          '-halt-on-error ' \
          '{texfile}'.format(
        compiler=compiler,
        texfile=filename,
        base_dir=base_dir,
    )
    pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = pipe.communicate()
    if out is not None:
        out = out.decode('utf-8')
    if err is not None:
      err = err.decode('utf-8')
    assert err is None or len(err) == 0

    errors = filter_tex_errors(out)

    filename_no_ext_no_path = os.path.splitext(os.path.basename(filename))[0]

    # Clear aux files
    r = os.popen('cd {base_dir} && rm {f}.log {f}.aux {f}.synctex.gz'.format(
        f=filename_no_ext_no_path,
        base_dir=base_dir,
        )
    )
    r.read()
    r.close()

    # Move to output folder
    if errors is None:
        created_pdf = os.path.abspath(base_dir + '/' + filename_no_ext_no_path + '.pdf')
        assert os.path.exists(created_pdf)
        target_pdf = os.path.abspath(result_dir + '/' + filename_no_ext_no_path + '.pdf')
        if target_pdf != created_pdf:
            cmd = 'cd {base_dir} && mv {created_pdf} {target_pdf}'.format(
                base_dir=base_dir,
                created_pdf=created_pdf,
                target_pdf=target_pdf,
            )
            r = os.popen(cmd)
            r.read()
            r.close()

    if errors is not None:
        print('Errors:')
        # print(out)
        print('\n'.join(errors))
        print('\n\n')
        raise RuntimeError('Failed to compile Tex file.')

    return target_pdf
