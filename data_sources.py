from typing import List

from clients import *

import sys
import warnings

from collections import defaultdict
from collections.abc import Iterable


# Finds and stores files.
# If names and/or types are given, the matched groups in each regex are added using the corresponding name/type.
# If no names are given, a tuple with all matches is added.
class Files:
    # Input:
    # 1. (base_folders, patterns):
    def __init__(self, base_folders=None, patterns=None, names=None, types=None, files=None, client=None):
        self.storage = []
        self.client = client or LocalHost()

        if files:
            self.storage.append(
                {
                    'idx_folder': 0,
                    'idx_pattern': 0,
                    'filename': f,
                    'file_description': f,
                } for f in make_list(files))

        if base_folders and patterns:
            for b, folder in enumerate(make_list(base_folders)):
                for p, pattern in enumerate(make_list(patterns)):
                    file_descriptions = self.client.list_files(base_folder=folder)

                    for file_description in file_descriptions:
                        description = dict()
                        filename = None

                        # Normal files
                        if isinstance(file_description, str):
                            filename = file_description

                        # Gitlab returns dict filenames with job-id, ...
                        if isinstance(file_description, dict):
                            filename = file_description['filename']

                        assert isinstance(filename, str)

                        # regular_files(file_descriptions): # TODO
                        if filename.endswith('#'):
                            warnings.warn('Invalid file "{}" ignored'.format(filename))
                            continue

                        matches = re.findall(pattern, filename)
                        if len(matches) > 0:
                            if isinstance(matches, list):
                                matches = matches[0]

                            # Add more information
                            description['idx_folder'] = b
                            description['idx_pattern'] = p
                            description['file_description'] = file_description
                            description['filename'] = filename

                            if names is not None:
                                if types is None:
                                    types = [str]*len(names)
                                for n, m, T in zip(names, matches, types):
                                    description[n] = T(m)
                            else:
                                description['matches'] = matches

                            self.storage.append(description)

            self.storage.sort(key=lambda desc: (desc['idx_folder'], desc['idx_pattern'], desc['filename']))

            # self.contents = [desc['content'] for desc in self.storage]
            # for desc in self.storage:
            #     del desc['content']

    def __str__(self):
        return self.filenames().__str__()

    def __iter__(self):
        return self.storage.__iter__()

    def __len__(self):
        return len(self.storage)

    def filenames(self):
        return [desc['filename'] for desc in self.storage]

    def contents(self):
        return [self.client.open(desc['file_description']) for desc in self.storage]

    def sort(self, key, reverse=False):
        self.storage.sort(key=key, reverse=reverse)
        return self

    def filter(self, func):
        result = Files()
        result.client = self.client

        condition = None
        if callable(func):
            condition = func
        if isinstance(func, dict):
            condition = lambda desc: all([desc[k] == func[k] for k in func.keys()])
        if isinstance(func, str):
            condition = lambda desc: len(re.findall(func, desc['filename'])) > 0
        assert condition

        result.storage = [desc for desc in self.storage if condition(desc)]
        return result

    def print(self):
        print(self)
        return self


# Class to manage a CSV data file.
class CSV:
    def __init__(self, filename=None, file=None):
        self.data = defaultdict(list)
        self.filename = filename
        if any([filename is not None, file is not None]):
            self.load(filename, file)

    def __str__(self):
        if self.filename:
            return 'CSV({})'.format(self.filename)
        else:
            return self.data.__str__()

    # Access to columns.
    def __getitem__(self, key: str):
        return self.data[key]

    def __setitem__(self, key: str, value):
        self.data[key] = value

    # Copy
    def copy(self):
        result = CSV()
        result.data = self.data.copy()
        result.filename = self.filename
        return result

    # Return the stored column names.
    def keys(self):
        return list(self.data.keys())

    def columns(self):
        return self.keys()

    # Forward values().
    def values(self):
        return self.data.values()

    # Iterable rows, returns a list of rows.
    def rows(self):
        result = [{k: self.row(r)[k][0] for k in self.keys()} for r in range(self.n_rows())]
        return result

    # Return the number of stored rows.
    def n_rows(self):
        if len(self.keys()) == 0:
            return 0
        assert all_same([len(self.data[key]) for key in self.keys()])
        return len(list(self.data.values())[0])

    # Load CSV from file.
    def load(self, filename, file=None, dequote=None, delimiter=' '):
        if dequote is None:
            def dequote(s):
                if s.startswith('{') and s.endswith('}'):
                    return s[1:-1]
                if s.startswith('"') and s.endswith('"'):
                    return s[1:-1]
                return s

        created = False
        if file is None:
            created = True
            file = open(filename, 'r')

        self.data = None
        csv_reader = csv.reader(file, delimiter=delimiter, quotechar='"')
        header = None
        for row in csv_reader:
            # Header
            if not header:
                # Last entry may be empty, remove it
                if row[-1] == '':
                    header = row[:-1]
                else:
                    header = row

                # Try to detect if no header is given
                if not all([isinstance(convert(h), str) for h in header]):
                    print('Warning: no header detected')
                    if len(header) == 2:
                        self.data = {'key': [convert(header[0])], 'value': [header[1]]}
                    else:
                        self.data = {'column_{}'.format(i): [convert(h)] for i, h in enumerate(header)}
                    header = list(self.data.keys())
                else:
                    # All fine
                    self.data = {c: [] for c in header}
            else:
                for i, k in enumerate(header):
                    value = convert(row[i])
                    # If entry is like {value}, remove braces.
                    if isinstance(value, str):
                        value = dequote(value)

                    self.data[k].append(value)

        if created:
            file.close()

        self.filename = filename
        return self

    # Write CSV to file.
    def output(self, filename, ordering=lambda cols: sorted(cols), quote=None, delimiter=' '):
        if quote is None:
            quote = lambda s: '{' + s + '}'

        self.filename = filename
        if self.n_rows() == 0:
            warnings.warn('Output empty table. Missing some data?', RuntimeWarning)

        columns = list(self.keys())
        if ordering is not None:
            assert callable(ordering)
            columns = ordering(columns)
            assert isinstance(columns, Iterable)

        dir = os.path.dirname(filename)
        if dir != '':
            os.makedirs(dir, exist_ok=True)

        with open(filename, 'w') as csvfile:
            min_size = 132456798
            if len(columns) == 0:
                return
            for key in columns:
                csvfile.write('{}{}'.format(key, delimiter))
                min_size = min(min_size, len(self.data[key]))

            for i in range(min_size):
                csvfile.write('\n')
                for key in columns:
                    value = self.data[key][i]
                    if isinstance(value, int):
                        csvfile.write('{}{}'.format(value, delimiter))
                        continue
                    if isinstance(value, float):
                        csvfile.write('{:.8e}{}'.format(value, delimiter))
                        continue
                    if isinstance(value, str):
                        # Pgfplots requires spaced strings to be embraced
                        assert callable(quote)
                        value = quote(value)
                        csvfile.write('{}{}'.format(value, delimiter))
                        continue
                    assert False

            csvfile.close()

        return self

    # Well, rename a column.
    def rename_column(self, column, new_column):
        if column not in self.keys():
            print('Warning: column', column, 'does not exist.')
        else:
            self.data[new_column] = self.data.pop(column)
        return self

    # Add a column using a generating function
    def add_column(self, name, function):
        if callable(function):
            self[name] = [function({key: value[0] for key, value in self.summary(row).data.items()})
                          for row in range(self.n_rows())]
        else:
            self[name] = [function]*self.n_rows()
        return self

    # Return the unique values in the given column
    def unique_values(self, column: str):
        return list(set(self[column]))

    # Add index column
    def add_index(self, name='index'):
        self[name] = list(range(self.n_rows()))
        return self

    # Remove a column
    def remove_column(self, *names):
        for name in names:
            self.data.pop(name, None)
        return self

    # Apply f to the given column
    def apply(self, f, columns=None):
        assert f
        assert callable(f)
        if columns is None:
            columns = self.keys()
        if isinstance(columns, str):
            columns = [columns]
        for c in columns:
            self.data[c] = list(map(f, self.data[c]))
        return self

    # Return the different groups according to group_by.
    # Returns a list of dicts with keys given by group_by and unique values.
    # If group_by is None, a list containing an empty dict is returned.
    # This ensures compatibility with csv.where(g) for g in csv.get_groups.
    def get_groups(self, group_by):
        group_by = make_list(group_by)
        if group_by == [None] or group_by is None:
            return [{}]

        group_by = [g for g in group_by if g in self.keys()]

        t = tuple(self[g] for g in group_by)
        lens = [len(a) for a in t]
        for ll in lens:
            assert ll == lens[0]
        tmp = list(zip(*t))
        groups = list(set(tmp))
        groups.sort()
        # Convert the list of tuples into a list of dicts with key given by group_by.
        groups = [{by: v for by, v in zip(group_by, group)} for group in groups]
        return groups

    def print(self, ordering=lambda cols: sorted(cols)):
        assert callable(ordering)
        keys = ordering(list(self.keys()))
        assert isinstance(keys, Iterable)

        sizes = {k: max(map(lambda x: len('{}'.format(x)), self[k] + [k])) for k in keys}

        for i in range(-2, self.n_rows()):
            if i == -2:
                line = keys
            else:
                if i == -1:
                    line = ['-'*(sizes[k] + 2) for k in keys]
                else:
                    line = [self[k][i] for k in keys]

            fmt = ''.join(['{{:^{}}}'.format(sizes[k] + 2) for k in keys])
            result = fmt.format(*line)
            result = result.replace('\n', '\\n')
            print(result)
        return self

    # Return CSV with columns matching any of the expressions given.
    # TODO deprecate
    def filter(self, *expressions, regex=True, negate=False) -> 'CSV':
        result = CSV()
        keys = self.keys()
        if regex:
            matched = [k for k in keys if any(matches(k, make_list(expressions)))]
        else:
            matched = [k for k in keys if k in make_list(expressions)]

        if negate:
            matched = set(keys) - set(matched)

        result.data = defaultdict(list, {k: self.data[k] for k in matched})
        return result

    def include(self, *expressions, regex=True) -> 'CSV':
        return self.filter(*expressions, regex=regex, negate=False)

    def exclude(self, *expressions, regex=True) -> 'CSV':
        return self.filter(*expressions, regex=regex, negate=True)

    # Return CSV with rows satisfying all conditions,
    # given by dict with key=fieldname, value=callable(str).
    # If no conditions are given (conditions == {}), the whole table is returned.
    # TODO where with condition getting full row
    def where(self, conditions) -> 'CSV':
        result = CSV()
        rows = find_rows(self.data, conditions)
        for col in self.keys():
            result[col] = [self.data[col][i] for i in rows]
        return result

    # Return CSV with rows sorted by given fields.
    def sort_by(self, *fields, reverse=False):
        result = CSV()
        result.data = self.data.copy()

        sort_by(result, [f for f in fields if f in result.keys()])

        if reverse:
            for k in self.keys():
                result[k] = list(reversed(result[k]))
        return result

    # Searches and expands tuples into its separate values.
    def resolve_tuples(self):
        if self.n_rows() == 0:
            return self

        keys = self.keys()
        for key in keys:
            t = self[key][0]
            if not isinstance(t, (int, float, str)):
                # List of tuples
                values = self[key]
                ls = [len(v) for v in values]
                assert all_same(ls)
                ls = ls[0]

                for i in range(ls):
                    self['{}_{}'.format(key, i)] = [v[i] for v in values]

                self.remove_column(key)
        return self

    # Reduces rows according to op on given field.
    # I.e. op=min, column=time returns the row with minimum time in each group.
    #
    # In case that the reduction does not reduce to a value already contained in the data,
    # a new row with the computed value is added. All other fields are either copied (column only has one value),
    # or the user needs to provide default values.
    #
    # If op returns a list/tuple, the new column names are create as column_i.
    def reduce(self, column, group_by=None, op=min, default_values=None):
        assert column in self.keys()
        assert callable(op)

        result = Tables()
        group_by = make_list(group_by)
        groups = self.get_groups(group_by)
        for g, group in enumerate(groups):
            rows = self.where(group)

            values = rows[column]
            value = op(values)
            row = rows.where({column: value})

            # If we dont have an exact match (i.e. when op == mean)
            if row.n_rows() != 1:
                # row = rows.summary(0)
                for col in rows.keys():
                    # Add reduced value
                    if col == column:
                        row[col].append(value)
                        continue

                    # If all rows have the same value, take that one ...
                    if all_same(rows[col]):
                        row[col].append(rows[col][0])
                    else:
                        # ... otherwise ask user to provide it.
                        if default_values is None:
                            raise Exception('Could not find exact match for reduced value.\n'
                                            'You need to provide default values to fill the row.\n'
                                            'Require default value for: {}'.format(col))

                        if col not in default_values:
                            raise Exception('Could not find exact match for reduced value.\n'
                                            'No defualt value given for: {}'.format(col))

                        row[col].append(default_values[col])

            result.storage.append({'data': row.copy()})

        return result.merge().resolve_tuples()

    # Remove rows that are not required for a good (linear) approximation of the graph (x,y).
    def downsampling(self, x: str, y: str, tolerance=1.0e-5):
        if 'expr' in x:
            data_x = parse_pgf_expression(x, csv=self)
        else:
            data_x = self[x]

        if 'expr' in y:
            data_y = parse_pgf_expression(y, csv=self)
        else:
            data_y = self[y]

        assert len(data_x) == len(data_y)

        # Assume that x values are increasing or decreasing
        assert strictly_decreasing(data_x) or strictly_increasing(data_x)

        if 'numpy' not in sys.modules:
            warnings.warn('Warning: numpy not available but required for downsampling.', RuntimeWarning)
            return self

        xmin = min(data_x)
        xmax = max(data_x)

        func = lambda p: np.interp(p, data_x, data_y)
        down_x, down_y = sample_function(func, [xmin, xmax], tol=tolerance)
        # Map to nearest available x-value
        down_data_x = take_closest_list(data_x, down_x)
        assert len(down_data_x) == len(down_x)
        down_data_x = sorted(list(set(down_data_x)))
        # Some x-values may end up being the same ...
        # assert len(down_data_x) == len(down_x)
        # print('Reduction:', len(data_x), '-->', len(down_data_x))

        selected_rows = self.where({x: lambda v: float(v) in down_data_x})

        assert isinstance(selected_rows, CSV)
        assert selected_rows.n_rows() == len(down_data_x)

        return selected_rows

    # Return CSV containing only selected rows.
    def row(self, r):
        return self.summary(row=r)

    def summary(self, row=-1):
        result = CSV()
        if isinstance(row, int):
            result.data = defaultdict(list, {k: [self.data[k][row]] for k in self.keys()})
        if isinstance(row, Iterable):
            result.data = defaultdict(list, {k: [self.data[k][r] for r in row] for k in self.keys()})
        return result

    # Remove duplicate rows.
    def unique(self):
        keys = self.data.keys()
        matrix = list(self.data.values())
        matrix = [list(i) for i in zip(*matrix)]  # Transpose
        matrix = [list(tline) for tline in set(tuple(line) for line in matrix)]
        matrix = [list(i) for i in zip(*matrix)]  # Transpose
        # Build csv
        result = CSV()
        result.data = defaultdict(list, {k: line for k, line in zip(keys, matrix)})
        return result

    # Return a list of column names that contain
    # more than one unique value
    def unique_columns(self) -> List[str]:
        return [c for c in self.columns() if len(set(self[c])) > 1]

    # Add information from the client to each row.
    def add_client_info(self, client, file_description=None):
        assert self.filename is not None or file_description is not None
        infos = client.informations(self.filename or file_description)
        for k in infos.keys():
            if k in self.keys():
                warnings.warn('Key "{}" already defined, skipped.'.format(k), RuntimeWarning)
            else:
                self.add_column(k, infos[k])
        return self


# Manages multiple CSV files.
class Tables(Files):
    # Input:
    # 1. (base_folders, patterns):
    #   Load CSV files found in any of the base folders matching any of the patterns.
    #   Index of base folder and pattern are added to the CSV.
    def __init__(self, base_folders=None, patterns=None, tables=None, files=None, client=None):
        Files.__init__(self, base_folders=base_folders, patterns=patterns, files=files, client=client)

        # Add found tables
        if len(self.storage) > 0:
            for description, file_content in zip(self.storage, self.contents()):
                file_description = description['file_description']
                csv = CSV().load(file_description, file_content)
                if hasattr(file_content, 'close'):
                    file_content.close()

                # Enrich the table with additional information
                csv['idx_folder'] = [description['idx_folder']]*csv.n_rows()
                csv['idx_pattern'] = [description['idx_pattern']]*csv.n_rows()

                description['data'] = csv

    def __str__(self):
        return self.filenames().__str__()

    def __getitem__(self, item):
        return self.storage[item]['data']

    def __setitem__(self, item, data):
        self.storage[item]['data'] = data

    def tables(self):
        return [s['data'] for s in self.storage]

    def print(self, mode=None):
        if mode is None:
            print(self)
            return self

        assert isinstance(mode, str)

        if mode == 'filenames':
            for t in self.tables():
                print(t.filename)
            return self

        if mode == 'keys':
            for t in self.tables():
                print(t.keys())
            return self

        raise Exception('Invalid mode.')

    # Reimplement sort, hide storage['data'] by providing it directly.
    def sort(self, key, reverse=False):
        tmp = Files.sort(self, key=lambda s: key(s['data']), reverse=reverse)
        result = Tables()
        result.storage = tmp.storage
        return result

    # Reimplement filter, hide storage['data'] by providing it directly.
    def filter(self, func):
        tmp = Files.filter(self, func=lambda s: func(s['data']))
        result = Tables()
        result.storage = tmp.storage
        return result

    # See CSV::add_client_info.
    def add_client_info(self, client):
        for description in zip(self.storage):
            file_description = description['file_description']
            if description['data'].filename is not None:
                assert description['data'].filename == file_description
            description['data'].add_client_info(client, file_description)

    # Return CSV with rows from each stored CSV.
    # Missing columns are filled with blank entries.
    def merge(self):
        keys = {k for t in self.storage for k in t['data'].keys()}
        result = CSV()
        result.data = defaultdict(list, {k: [] for k in keys})
        # result['idx_table'] = []

        for i, store in enumerate(self.storage):
            t = store['data']
            available = set(t.keys())
            missing = keys.difference(available)

            # Make sure all cols have equally many entries
            length = t.n_rows()

            # Copy data
            for col_name in available:
                result[col_name].extend(t[col_name])

            # Add dummy data
            for col_name in missing:
                result[col_name].extend([0]*length)

        # Make sure all cols have equally many entries
        result.n_rows()

        return result

    # Return CSV with selected row from each stored CSV.
    # Calls CSV.summary.
    def summary(self, row=-1):
        # Make sure all entries have same last line (-1)
        if row == -1:
            n_lines = []
            for tmp in self.storage:
                n_lines.append(len(list(tmp['data'].data.values())[0]))
            assert min(n_lines) == max(n_lines)

        summaries = Tables()
        summaries.storage = self.storage.copy()
        for t in summaries.storage:
            t['data'] = t['data'].summary(row)
        return summaries.merge()

    # Remove rows that are not required for a good (linear) approximation of the graph (x,y),
    # for each stored table.
    def downsampling(self, x: str, y: str, tolerance=1.0e-3):
        for desc in self.storage:
            desc['data'] = desc['data'].downsampling(x=x, y=y, tolerance=tolerance)
            desc['filename'] = None
        return self

    def foreach(self, method):
        for s in self.storage:
            s['data'] = method(s['data'])
        return self

    def output(self, filename=None):
        if filename is None:
            filename = lambda i: '{tmp_dir}/{filename}_{i}.csv'.format(
                tmp_dir=os.path.abspath('tmp/'),
                filename=str(hash(self.storage[i]['filename'])),
                i=i,
            )

        assert callable(filename)
        for i, desc in enumerate(self.storage):
            name = filename(i)
            desc['data'].output(name)
            desc['filename'] = name
        return self


# # Returns tuples of (cod.csv, results.csv)
# class CODFiles:
#     def __init__(self, base_folders, pattern=None):
#         self.base_folders = make_list(base_folders)
#         self.pattern = ['(?<!_)run.+cod.csv', '(?<!_)run.+results.csv'] if pattern is None else make_list(pattern)
#         assert len(self.pattern) == 2
#         self.storage = []
#
#     def get_files(self):
#         self.storage = [find_files(self.base_folders, p) for p in self.pattern]
#         self.storage = [regular_files(F) for F in self.storage]
#         self.storage = list(zip(self.storage[0], self.storage[1]))
#         return self.storage


class VtkFiles(Files):
    def __init__(self, base_folders=None, t=None, client=None):
        Files.__init__(
            self,
            base_folders=base_folders,
            client=client,
            patterns=r'solution_([0-9]+)\.pvtu',
            types=(int,),
            names=('t',),
        )

        if len(self) == 0:
            warnings.warn('Warning: no vtk files found', RuntimeWarning)
        else:
            # Get last timestep
            if t is None:
                ts = [(desc['t']) for desc in self.storage]
                t = max(ts)
                # print('Select timestep {}'.format(t))

            # Select files with timestep in t.
            if t and self.storage and self.contents:
                self.storage, self.contents = zip(*[(desc, cont) for desc, cont in zip(self.storage, self.contents())
                                                    if int(desc['t']) in make_list(t)])

            assert len(self.storage) > 0

        # return self.storage
