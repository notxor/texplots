from clients import Client
from helpers import any_matches

import requests
import zipfile
import io
import os.path
from dateutil.parser import parse


class GitlabClient(Client):
    def __init__(self, project_id:int=None, job_name=None, token=None):
        """
        Interface to Gitlab server.
        :param project_id: The integer id of the project.
        :param job_name: The jobname to work on. Can be set by Gitlab.jobname().
        :param token: Access token identifying the user, see Gitlabs user settings/access token.
        """
        super().__init__()

        self.project_id = project_id
        self.job_name = job_name
        self.token = token

        self.storage = []

    def __del__(self):
        self.disconnect()

    def jobname(self, jobname):
        """
        Set the jobname.
        :param jobname:
        :return:
        """
        self.job_name = jobname
        return self


    def jobs(self):
        """
        Return a list of jobs.
        :return:
        """
        return self.get_json('jobs')

    def matching_jobs(self):
        """
        Returns a list of jobs matching self.jobname.
        :return:
        """
        info = self.jobs()
        if self.job_name is not None:
            info = [d for d in info if 'name' in d and any_matches(d['name'], self.job_name)]
        return info

    def get_json(self, request):
        """
        Retrieve a json object, e.g., jobs or pipelines.
        :param request: Example jobs, pipelines, ...
        :return:
        """
        r = self.get(request)
        result = r.json()
        r.close()
        return result

    def delete_pipeline(self, request):
        """
        Delete the pipeline given in request.
        Example:
        for p in Gitlab.get_json('pipelines'):
            Gitlab.delete_pipeline(p['id'])
        :param request: id of the pipeline to delete.
        :return:
        """
        self.delete('pipelines/{request}'.format(request=request))

    # Fill the storage.
    def connect(self):
        if len(self.storage) > 0:
            return

        jobs = self.matching_jobs()

        for job in jobs:
            if not job['status'] == 'success':
                continue

            if len(job['artifacts']) == 0:
                continue

            has_zip = False
            for a in job['artifacts']:
                if a['filename'] == 'artifacts.zip':
                    has_zip = True
                    break

            if not has_zip:
                continue

            r = self.get('jobs/{job_id}/artifacts'.format(job_id=job['id']))
            zipped_file = zipfile.ZipFile(io.BytesIO(r.content))
            r.close()

            job['zip'] = zipped_file
            self.storage.append(job)

    def disconnect(self):
        self.storage = []

    def open(self, file_description):
        assert isinstance(file_description, dict)
        keys = list(file_description.keys())
        keys.remove('filename')
        keys.remove('job')
        for job in self.storage:
            if all([job[key] == file_description[key] for key in keys]):
                zipped_file = job['zip']
                assert file_description['filename'] in zipped_file.namelist()
                return io.TextIOWrapper(zipped_file.open(file_description['filename']), newline=None)
        pass

    def list_files(self, base_folder=None):
        self.connect()

        result = []
        for job in self.storage:
            zipped_file = job['zip']
            files = zipped_file.namelist()
            if base_folder is not None:
                files = [f for f in files if os.path.commonprefix([f, base_folder]) == base_folder]

            files = [{'filename': f, 'id': job['id'], 'job': job} for f in files]

            result.extend(files)

        return result

    def is_remote(self):
        return True

    def informations(self, file_description):
        job = file_description['job']
        dateformat = '%Y-%m-%d %H:%M'
        return {
            'filename': file_description['filename'],
            'job_id': job['id'],
            'commit': job['commit']['short_id'],
            'message': job['commit']['message'],
            'runner': job['runner']['description'],
            'name': job['name'],
            'stage': job['stage'],
            'status': job['status'],
            'started_at': parse(job['started_at']).strftime(dateformat),
            'committed_date': parse(job['commit']['committed_date']).strftime(dateformat),
        }

    def get(self, request, branch=None):
        """
        Post a GET request for the given project id and acess token.
        :param request: Examples: pipelines, jobs, ...
        :param branch: The branch to work on, default = master.
        :return:
        """
        if branch is None:
            branch = 'master'

        url = 'https://gitlab.com/api/v4/projects/{project_id}/{request}?per_page=100'
        url = url.format(
            project_id=self.project_id,
            branch=branch,
            job_name=self.job_name,
            request=request,
        )
        return requests.get(url, headers={'PRIVATE-TOKEN': self.token}, verify=True, stream=True)

    def post(self, request, branch=None):
        """
        Post a POST request for the given project id and access token.
        :param request:
        :param branch: The branch to work on, default = master.
        :return:
        """
        if branch is None:
            branch = 'master'

        url = 'https://gitlab.com/api/v4/projects/{project_id}/{request}'
        url = url.format(
            project_id=self.project_id,
            branch=branch,
            job_name=self.job_name,
            request=request,
        )
        return requests.post(url, headers={'PRIVATE-TOKEN': self.token}, verify=True, stream=True)

    def delete(self, request, branch=None):
        if branch is None:
            branch = 'master'

        url = 'https://gitlab.com/api/v4/projects/{project_id}/{request}'
        url = url.format(
            project_id=self.project_id,
            branch=branch,
            job_name=self.job_name,
            request=request,
        )
        r = requests.delete(url, headers={'PRIVATE-TOKEN': self.token}, verify=True, stream=True)
        r.close()