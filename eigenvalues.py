#!/usr/bin/env python3

from data_sources import *


try:
    from spectrum import *
    _spectrum_available_ = True

except ImportError:
    warnings.warn('Warning: SciPy/Numpy not available for eigenvalue computations.', ImportWarning)
    _spectrum_available_ = False


class SparseEigenvalues:
    def __init__(self, files: Files, n_low: int = 1, n_high: int = 1):
        self.files = files
        self.n_low = n_low
        self.n_high = n_high

    def generate(self):
        csv = CSV()

        assert len(self.files.storage) > 0

        for i, (d, content) in enumerate(zip(self.files.storage, self.files.contents())):
            f = d['filename']
            t = d['t']
            b = d['block']
            lvl = d['lvl']

            if _spectrum_available_:
                result = analyze(f)
            else:
                result = dict()
                result['PA'] = [0]
                result['A'] = [0]

            for k, v in d.items():
                csv.data[k].append(v)

            values = result['PA']
            if len(values) > 0:
                csv.data['scaled_min'].append(min([v.real for v in values]))
                csv.data['scaled_min_imag'].append(min([v.imag for v in values]))
                csv.data['scaled_max'].append(max([v.real for v in values]))
                csv.data['scaled_max_imag'].append(max([v.imag for v in values]))
            else:
                warnings.warn('Warning: empty value list', RuntimeWarning)

            values = result['A']
            if len(values) > 0:
                csv.data['min'].append(min([v.real for v in values]))
                csv.data['min_imag'].append(min([v.imag for v in values]))
                csv.data['max'].append(max([v.real for v in values]))
                csv.data['max_imag'].append(max([v.imag for v in values]))
            else:
                warnings.warn('Warning: empty value list', RuntimeWarning)

            # if not _scipy_available_:
            #     for k, v in d.items():
            #         csv.data[k].append(v)
            #     csv.data['min'].append(0)
            #     csv.data['max'].append(0)
            #     csv.data['rowsum'].append(0)
            #     for i in range(self.n_high + self.n_low):
            #         csv.data['e{}'.format(i)].append(values[i])
            #
            # if _scipy_available_:
            #     [data, row, column] = np.loadtxt(content)
            #     n = int(max(row)) + 1
            #     m = int(max(column)) + 1
            #     original_sparse_matrix = sp.sparse.csr_matrix((data, (row, column)), shape=(m, n))
            #
            #     diag = original_sparse_matrix.diagonal()
            #     diag = sp.sparse.spdiags(diag, 0, len(diag), len(diag))
            #
            #     if self.n_low > 0:
            #         low, _ = la.eigs(original_sparse_matrix, M=diag, k=self.n_low, sigma=0.0)
            #         low_orig, _ = la.eigs(original_sparse_matrix, k=self.n_low, sigma=0.0)
            #     else:
            #         low = np.ndarray(shape=(0,))
            #         low_orig = np.ndarray(shape=(0,))
            #
            #     if self.n_high > 0:
            #         high, _ = la.eigs(original_sparse_matrix, M=diag, k=self.n_high)
            #         high_orig, _ = la.eigs(original_sparse_matrix, k=self.n_high)
            #     else:
            #         high = np.ndarray(shape=(0,))
            #         high_orig = np.ndarray(shape=(0,))
            #
            #     values = np.concatenate((low, high))
            #     values.sort()
            #     values = [v for v in values]
            #
            #     values_orig = np.concatenate((low_orig, high_orig))
            #     values_orig.sort()
            #     values_orig = [v for v in values_orig]
            #
            #     # row_sum = la.norm(sparse_matrix, ord=1)
            #     # row_sum_orig = la.norm(original_sparse_matrix, ord=1)
            #
            #     print('Scaled:', t, lvl, b, min(values), max(values))
            #     print('Original', t, lvl, b, min(values_orig), max(values_orig))
            #
            #     for k, v in d.items():
            #         csv.data[k].append(v)
            #     csv.data['scaled_min'].append(min([v.real for v in values]))
            #     csv.data['scaled_min_imag'].append(min([v.imag for v in values]))
            #     csv.data['scaled_max'].append(max([v.real for v in values]))
            #     csv.data['scaled_max_imag'].append(max([v.imag for v in values]))
            #
            #     csv.data['min'].append(min([v.real for v in values_orig]))
            #     csv.data['min_imag'].append(min([v.imag for v in values_orig]))
            #     csv.data['max'].append(max([v.real for v in values_orig]))
            #     csv.data['max_imag'].append(max([v.imag for v in values_orig]))
            #
            #     # csv.data['rowsum'].append(row_sum.real)
            #     # csv.data['rowsum_imag'].append(row_sum.imag)
            #     for i in range(len(values)):
            #         csv.data['scaled_e{}'.format(i)].append(values[i].real)
            #         csv.data['scaled_e{}_imag'.format(i)].append(values[i].imag)
            #
            #         csv.data['e{}'.format(i)].append(values_orig[i].real)
            #         csv.data['e{}_imag'.format(i)].append(values_orig[i].imag)

        return csv


if __name__ == "__main__":
    files = Files(base_folders='.', patterns=sys.argv[1:])
    E = SparseEigenvalues(files=files)
    csv = E.generate()
    csv.print()
