#!/usr/bin/python3

from helpers import *

import os.path
import sys
import getopt

if __name__ == "__main__":
    try:
        optlist, args = getopt.gnu_getopt(sys.argv[1:], 'o:')
    except getopt.GetoptError:
        sys.exit(2)

    output = None
    for opt, arg in optlist:
        if opt == '-o':
            output = arg

    # Unique folders
    base_folders = set(args)

    files = [os.path.join(dir, 'results.csv') for dir in find_subfolders(base_folders)]
    files = [f for f in files if os.path.isfile(f)]

    S = parameter_summary(files, ['lvl', 'run', 'h', 'eps', 'dofs', 'cells'])

    if output is not None:
        write_csv(S, output)
    else:
        S.print()
