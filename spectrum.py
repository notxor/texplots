#!/usr/bin/env python3

import numpy as np
import scipy as sp

import sys

import scipy.sparse.linalg as la
from scipy.sparse import eye


def to_real(c):
    if abs(c.imag) < 1.0e-8:
        return float(c.real)
    else:
        return c


def to_str(values):
    tmp = ['{:.2e}'.format(e.real) for e in values]
    # tmp = ['{:.2e} + {:.2e}i'.format(e.real, e.imag) for e in values]
    return '\t'.join(tmp)


def load_matrix(filename):
    data = np.loadtxt(filename)
    [data, row, column] = data
    n = int(max(row)) + 1
    m = int(max(column)) + 1
    return sp.sparse.csr_matrix((data, (row, column)), shape=(n, m))


def is_diagonal(m):
    d = m.diagonal()
    d = sp.sparse.spdiags(d, 0, len(d), len(d))
    nnz = (m - d).count_nonzero()
    return nnz == 0


# Try to find reliable estimates of the largest and smallest eigenvalues.
# If ignore_ones is set, we try to find eigenvalues different from 1.
# This is usefull in case of (homogeneous) constraints, that add a spurious 1 eigenvalue.
def extremal_eigenvalues(operator, symmetric=False, ignore_ones=True, **eig_args):
    eig = la.eigsh if symmetric else la.eigs

    result = []

    operator.eliminate_zeros()

    # Diagonal matrix
    if is_diagonal(operator):
        return [operator.min(), operator.max()]

    # Search reliable smallest eigenvalue (possibly different to 1)
    try:
        k = 2
        while True:
            if k >= operator.shape[0] - 1:
                break

            E, _ = eig(A=operator, k=k, sigma=0, **eig_args)
            if ignore_ones:
                E = [v for v in E if abs(v - 1.0) > 1.0e-2]
            if len(E) >= 2:
                break
            k += 2

        E.sort()
        if len(E) >= 2:
            assert E[0] <= E[1]
            result.append(E[0])
            result.append(E[1])
        if len(E) == 1:
            result.append(E[0])
    except sp.sparse.linalg.eigen.arpack.ArpackError:
        print('Warning: could not compute eigenvalues.')

    # Search reliable largest eigenvalue (possibly different to 1)
    try:
        k = 2
        while True:
            if k >= operator.shape[0] - 1:
                break

            E, _ = eig(A=operator, k=k, **eig_args)
            if ignore_ones:
                E = [v for v in E if abs(v - 1.0) > 1.0e-2]
            if len(E) >= 2:
                break
            k += 2

        E.sort()
        if len(E) >= 2:
            assert E[-2] <= E[-1]
            result.append(E[-2])
            result.append(E[-1])
        # result.append(max(E))

    except sp.sparse.linalg.eigen.arpack.ArpackError:
        print('Warning: could not compute eigenvalues.')

    if len(result) > 1:
        assert result[0] <= result[1]
    return result


def print_result(r):
    result = r.copy()

    keys = ['A', 'PA']

    # for k in keys:
        # result[k] = list(set(result[k]))
        # result[k].sort()
        # result[k] = [v for v in result[k] if abs(v - 1.0) > 1.0e-5]
        # result[k] = [result[k][0]] if len(result[k]) > 0 else []

    max_len = max([len(values) for key, values in result.items()])
    print(*['{:^15}'.format(k) for k in keys])
    for row in range(max_len):
        vals = ['{:^15.4e}'.format(to_real(result[k][row])) if len(result[k]) > row else '' for k in keys]
        print(*vals)


# def estimate_spectrum(filename):
#     A = load_matrix(filename)
#
#     AT = A.transpose()
#     D = A - AT
#     ns = 0.5 * D.max()
#     symmetric = ns == 0.0
#     if not symmetric:
#         print('Non-symmetry: {:.2e}'.format(ns))
#
#     eig = la.eigsh if symmetric else la.eigs
#
#     diag = A.diagonal()
#     inv_diag = 1.0 / diag
#     diag = sp.sparse.spdiags(diag, 0, len(diag), len(diag))
#     inv_diag = sp.sparse.spdiags(inv_diag, 0, len(inv_diag), len(inv_diag))


def analyze(filename=None, A=None):
    result = dict()
    result['A'] = list()
    result['PA'] = list()
    # result['M'] = list()

    if filename is not None:
        A = load_matrix(filename)
    else:
        assert A is not None

    AT = A.transpose()
    D = A - AT
    ns = 0.5 * D.max()
    symmetric = ns == 0.0
    if not symmetric:
        print('Non-symmetry: {:.2e}'.format(ns))

    eig = la.eigsh if symmetric else la.eigs

    diag = A.diagonal()
    inv_diag = 1.0 / diag
    diag = sp.sparse.spdiags(diag, 0, len(diag), len(diag))
    inv_diag = sp.sparse.spdiags(inv_diag, 0, len(inv_diag), len(inv_diag))

    # print(filename)

    E = extremal_eigenvalues(A, symmetric=symmetric)
    result['A'].extend(E)

    # # # M = I - D^-1 A = D^{-1} (D - A)
    # M = sp.sparse.eye(A.shape[0]) - inv_diag * A
    # # M = diag - A
    # E = extremal_eigenvalues(M, ignore_ones=False)
    # result['M'].extend(E)

    PA = inv_diag * A
    # E = extremal_eigenvalues(PA)
    E = extremal_eigenvalues(A, symmetric=symmetric, M=diag)
    result['PA'].extend(E)
    # E1, _ = eig(A, k=1, M=diag, sigma=0)
    # E2, _ = eig(A, k=1, M=diag)
    # E = np.concatenate((E1, E2))
    # E.sort()
    # # print('PA ', to_str(E))
    # result['PA'].extend(E)

    # sqrt_inv_diag = np.sqrt(inv_diag)
    # sqrt_inv_diag = sp.sparse.spdiags(sqrt_inv_diag, 0, len(sqrt_inv_diag), len(sqrt_inv_diag))
    # PAP = sqrt_inv_diag * A * sqrt_inv_diag
    # E = extremal_eigenvalues(PAP, symmetric)
    # print('PAP', to_str(E))

    return result


if __name__ == "__main__":
    filenames = sys.argv[1:] if len(sys.argv) >= 2 else ['test.txt']

    for filename in filenames:
        print()
        print()
        print(filename)
        print()
        result = analyze(filename)
        print_result(result)
