import random
import string

from tex import *
from data_sources import *
from fitting import *
import configuration


class Curve:
    """
    Description of a possibly parametric curve.
    Stores:
    * x: str. Note that 'x' is also the curve parameter, not 't'.
    * y: str
    * domain: str|tuple(2)
    * legend: str
    * options: special plot options for this curve
    """

    def __init__(self, x: str = None, y: str = None, domain=None, legend: str = None, options=None):
        assert y is not None

        self.x = x or 'x'
        self.y = y
        self.legend = legend
        self.domain = domain
        self.options = options


# Plot types:
# Single file:
# * x, [y]
#
# Multiple files:
# * summary to single file
# * x, [y]

class RawPlot:
    """
    Plot (parametric) curves without underlying data.

    Options:
    There are various ways to modify the curves.
    * axis_opts defines overall options for all plots, axis, etc.
    * plot_opts defines general options for the curves, but can be overridden by the curves own options.

    Inputs:
    * name
    * curves        list of Curve objects
    * axis_opts     axis options
    * plot_opts     prepended to each curve but after default options
    """

    def __init__(self, name='',
                 curves=None,
                 axis_opts=None, plot_opts=None, style_index=None,
                 ):
        self.name = name
        self.curves = curves

        if axis_opts is not None:
            self.axis_opts = axis_opts

        if plot_opts is not None:
            assert isinstance(plot_opts, list)
        self.plot_opts = plot_opts or []
        self.style_index = style_index

    def __str__(self):
        return 'RawPlot[{}]'.format(self.name)

    def generate(self, base_path='', tex=None):
        assert self.curves

        if tex is None:
            tex = configuration.Default.new_tex()
            tex.set_axis_opts(self.axis_opts)

        for c in self.curves:
            options = []
            if c.domain is not None:
                if isinstance(c.domain, str):
                    options.append('domain={}'.format(c.domain))
                if isinstance(c.domain, tuple):
                    options.append('domain={}:{}'.format(c.domain[0], c.domain[1]))

            if self.plot_opts is not None:
                options += self.plot_opts

            if c.options is not None:
                assert isinstance(c.options, list)
                options += c.options

            tex.add_parametric_plot(
                x=c.x, y=c.y,
                options=options,
                legend=c.legend,
                # style=index,
            )

        return tex


# Use MultiPlot (or derived classes) for most standard plots,
# that dont require the computation of additional quantities.
# It is mainly good to plot (multiple) lines given x,y expressions.
class MultiPlot:
    """
    Versatile plotting class.

    Features:

    * Options:
        Settings can be provided using
            @param axis_opts(str, array, tuple) and
            @param plot_opts(str, array, tuple).

    * Groups:
        Split data according to @param group_by(str, tuple) to obtain multiple plots.
        Groups can be ignored by @param ignore(dict(str,str), lambda(group)),
        included by @param include(dict(str,str))
        and sorted via @param sort_by(str, tuple).
        - @param ignore is None, @param include is not None:
            plot is included <=> include(group) == True
        - @param ignore is not None, @param include is None:
            plot is included <=> ignore(group) == False
        - @param ignore is not None, @param include is not None:
            plot is included <=> ignore(group) == False and include(group) == True

    * Fitting:
        Adds curves fitted to the x,y-plot of each group by providing @param fitting.
        Plot options can be controlled via @param fitting_options.
        If @param fitting is of type Fit, then the given model is applied to all plots.
        If @param fitting is a callable, it is supposed to return a Fit object for each group given,
        or None, if no fitting be done for a group.
    """

    def __init__(self, name='', source=None,
                 x=None, y=None,

                 # Groups:
                 ignore=None, include=None,
                 group_by=None, sort_by=None, reverse=False,

                 legend=None,

                 # Options:
                 axis_opts=None, plot_opts=None, style_index=None,

                 additional_plots=None,

                 # Fitting:
                 fitting=None, fitting_options=None,
                 ):

        assert source is not None

        self.name = name
        self.source = source

        self.x = make_list(x)
        self.y = make_list(y)

        self.group_by = make_list(group_by)
        self.sort_by = sort_by
        self.reverse = reverse
        self.ignore = ignore
        self.include = include

        self.additional_plots = additional_plots

        self.fitting = fitting
        self.fitting_options = fitting_options or ['black', 'densely dotted', 'mark=none']
        self.fitting_options = make_list(self.fitting_options)

        self.axis_opts = axis_opts
        self.plot_opts = plot_opts
        self.style_index = style_index

        self.legend = legend

    def __str__(self):
        return 'MultiPlot[{}]'.format(self.name)

    def is_included(self, group):
        assert self.ignore is None or isinstance(self.ignore, dict) or callable(self.ignore)
        assert self.include is None or isinstance(self.include, dict) or callable(self.include)
        ignore = False
        if callable(self.ignore):
            ignore = self.ignore(group)
        if isinstance(self.ignore, dict):
            ignore = any(matches_dict(group, self.ignore))

        include = True
        if callable(self.include):
            include = self.include(group)
        if isinstance(self.include, dict):
            include = any(matches_dict(group, self.include))

        return include and not ignore

    def generate(self, base_path='', tex=None):
        assert self.x
        assert self.y
        assert self.group_by

        if tex is None:
            tex = configuration.Default.new_tex()
            tex.set_axis_opts(self.axis_opts)

        # Collect csvs and filenames
        csvs = None
        if isinstance(self.source, CSV):
            csvs = [self.source]
        if isinstance(self.source, Tables):
            csvs = self.source.tables()

        for csv in csvs:
            assert csv.n_rows() > 0

        # Generate all groups
        groups = self.make_all_groups(csvs=csvs)
        assert len(groups) > 0

        # Write data for each group to a separate file.
        # Speeds up tex compilation A LOT!
        filenames = []
        for group_idx, group in enumerate(groups):
            token = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
            old_f = group['filename']
            if old_f is not None:
                folder = os.path.dirname(old_f)
                filename = os.path.splitext(os.path.basename(old_f))[0]
                ext = os.path.splitext(old_f)[-1]
                f = f'{folder}/_group_{group_idx}_{filename}_{token}{ext}'
            else:
                f = f'_group_{group_idx}_tmp_{token}.csv'
            filenames.append(f)
            group['data'].output(f)
            group['filename'] = f

        tex.add_tables(filenames)

        # Plot each group
        for group_idx, group in enumerate(groups):
            x = group['x']
            y = group['y']

            index = None
            if callable(self.style_index):
                index = self.style_index(group)

            tex.add_plot(
                x=x, y=y,
                table=group['filename'],
                options=self.make_options(group=group),
                legend=self.make_legend(tex=tex, group=group),
                style=index,
            )

        # Do the fitting separately at the end
        for group in groups:
            if self.fitting is not None:
                self.make_fit(tex=tex, group=group)

        if self.additional_plots is not None:
            self.make_additional_plots(tex=tex, base_path=base_path)

        return tex

    def prepare_groups(self, groups):
        # Add min/max values of ints and floats:
        # List of fields in the group
        if len(groups) > 0:
            fields = list(groups[0].keys())
            field_values = {f: [g[f] for g in groups] for f in fields}
            for f in fields:
                values = field_values[f]
                if isinstance(values[0], (int, float)):
                    for group in groups:
                        group['_max_' + f] = max(values)
                        group['_min_' + f] = min(values)

        # Remove unwanted groups:
        groups = [g for g in groups if self.is_included(g)]

        # Sort groups:
        if self.sort_by is not None:
            if callable(self.sort_by):
                groups.sort(key=self.sort_by, reverse=self.reverse)
            else:
                if isinstance(self.sort_by, str):
                    groups.sort(key=lambda x: x[self.sort_by], reverse=self.reverse)
                else:
                    assert isinstance(self.sort_by, Iterable)
                    groups.sort(key=lambda x: tuple(x[i] for i in self.sort_by), reverse=self.reverse)

        return groups

    # Create all the different groups and enrich them with infos.
    def make_all_groups(self, csvs):
        groups = []
        for idx_table, desc in enumerate(csvs):
            # Get data
            csv = None
            if isinstance(desc, dict):
                csv = desc['data']
            else:
                if isinstance(desc, CSV):
                    csv = desc
            assert isinstance(csv, CSV)

            csv_groups = self.make_single_groups(csv=csv)

            # Add file info
            for group in csv_groups:
                group['idx_table'] = idx_table
                group['filename'] = csv.filename

            groups.extend(csv_groups)

        return self.prepare_groups(groups)

    # Create all the different groups for a single CSV and enrich them with infos.
    def make_single_groups(self, csv):
        groups = []

        # Add some info about all other groups to each group.
        # In particular, for int fields, add the max value (later on)
        # Also add the data.
        data_groups = csv.get_groups(self.group_by)
        for group in data_groups:
            group['data'] = csv.where(group)
            for idx_y, y in enumerate(self.y):
                for idx_x, x in enumerate(self.x):
                    group['x'] = x
                    group['y'] = y
                    group['idx_x'] = idx_x
                    group['idx_y'] = idx_y
                    groups.append(group.copy())

        return groups

    def make_options(self, group):
        # discard = ', '.join(
        #     ['discard if not={{{field}}}{{{value}}}'.format(field=f, value=group[f]) for f in self.group_by if
        #      f is not None and group[f] is not None])

        options = []
        if self.plot_opts is None:
            return options
        if callable(self.plot_opts):
            return options + make_list(self.plot_opts(group))
        if isinstance(self.plot_opts, list):
            return options + self.plot_opts
        if isinstance(self.plot_opts, str):
            return options + [self.plot_opts]
        raise Exception('Invalid plot_opts given!')

    # Create the legend for the given group
    def make_legend(self, tex, group):
        assert self.legend is None or callable(self.legend) or isinstance(self.legend,
                                                                          (tuple, list, str))
        legend = None
        if callable(self.legend):
            legend = self.legend(group)
        if isinstance(self.legend, (tuple, list)):
            raise Exception('Giving fixed legends is not stable, consider a lambda expression instead')
        if isinstance(self.legend, str):
            legend = self.legend
        return legend

    # Fit the plots
    def make_fit(self, tex, group):
        x = group['x']
        y = group['y']
        csv = group['data']

        if 'expr' in x:
            x_values = parse_pgf_expression(x, csv=csv)
        else:
            x_values = group['data'][x]

        if 'expr' in y:
            y_values = parse_pgf_expression(y, csv=csv)
        else:
            y_values = group['data'][y]

        fit = []

        def add_fit(f):
            if f is None:
                return
            if isinstance(f, Fit):
                fit.append(f)

        for f in make_list(self.fitting):
            if callable(f):
                args = make_list(f(group))
                for n in args:
                    add_fit(n)
            else:
                add_fit(f)

        for f in fit:
            f.fit(xdata=x_values, ydata=y_values)
            expression = f.tex_expression()

            if expression is None:
                warnings.warn('Warning: could not fit given data points', RuntimeWarning)
            else:
                tex.add_plot(
                    y=expression,
                    options=self.fitting_options + [
                        'domain={}:{}'.format(min(x_values), max(x_values))],
                    legend=f.legend(),
                )

    def make_additional_plots(self, tex, base_path):
        for a in make_list(self.additional_plots):
            if isinstance(a, str):
                tex.add_plot(raw=a)
                continue
            # TODO move to config level
            if isinstance(a, MultiPlot):
                a = a.generate(base_path=base_path)
                tex.add(a)
                continue
            if isinstance(a, Tex):
                tex.add(a)
                continue
            raise Exception('Invalid additional_plots given!')


# Very basic class, intended to be derived from for more sophisticated plots.
class NewPlot:
    def __init__(self, name, source, x: str, y, axis_opts=None, plot_opts=None):
        self.name = name

        # Convert source to Tables object
        if isinstance(source, Tables):
            self.source = source
        else:
            if isinstance(source, CSV):
                self.source = Tables(tables=source)
            else:
                assert False

        self.x = x
        self.y = make_list(y)

        self.axis_opts = []
        self.axis_opts.append('grid=both')
        self.axis_opts.append('grid style={dotted, gray}')
        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)

        self.plot_opts = ['mark=none']
        if plot_opts is not None:
            self.plot_opts.extend(plot_opts)

        self.tex = None

    def add_plot(self, y, table):
        self.tex.add_plot(
            x=self.x, y=y,
            table=table,
            options=self.plot_opts
        )

    def add_plots(self, tables):
        for table in tables:
            for single_y in self.y:
                self.add_plot(single_y, table)

    def generate(self):
        self.tex = configuration.Default.new_tex()
        tables = self.source.filenames()

        assert tables is not None
        for t in tables:
            assert t is not None

        self.tex.add_tables(tables)
        self.tex.set_axis_opts(self.axis_opts)
        self.add_plots(tables)

        return self.tex


class Timing:
    def __init__(self, name='', source: CSV = None, fieldname=None, axis_opts=None, plot_opts=None, group_by='run',
                 legend=None, style_index=None):
        self.name = name
        self.source = source
        self.x = 'n_ranks'
        self.y = fieldname
        self.group_by = make_list(group_by)
        self.style_index = style_index
        self.legend = legend

        self.axis_opts = []
        self.axis_opts.append(r'xlabel={\# cores}')
        self.axis_opts.append('ylabel={Time $[s]$}')
        self.axis_opts.append('xtick=data, xmode=log, log basis x={2}')
        self.axis_opts.append('ymode=log, log basis y={2}')
        self.axis_opts.append('log ticks with fixed point')
        self.axis_opts.append('legend pos = south west')
        self.axis_opts.append('grid=both')
        self.axis_opts.append('grid style={dotted, gray}')

        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)

        self.plot_opts = plot_opts

    def __str__(self):
        return 'Timing[{}]'.format(self.name)

    def generate(self, base_path=''):
        assert self.y is not None

        csv = self.source
        tables = [self.source.filename]

        # Throw away ignored groups
        # Build groups = list(dict()...)
        groups = csv.get_groups(self.group_by)

        # if callable(self.ignore):
        #     groups = [d for d in groups if self.ignore(d)]
        # if isinstance(self.ignore, dict):
        #     groups = [d for d in groups if not any(matches_dict(d, self.ignore))]
        #
        # Sort groups:
        # if self.sort_by is not None:
        #     groups.sort(key=self.sort_by)

        tex = configuration.Default.new_tex()
        assert tables
        assert len(tables) == 1

        ranks = set(map(int, csv['n_ranks']))
        tex.set_axis_opts(
            self.axis_opts + ['xtick={{{ticks}}}'.format(ticks=','.join([str(r) for r in sorted(ranks)]))])
        tex.add_tables(tables)

        leg_count = 0

        for group_idx, group in enumerate(groups):
            discard = ', '.join(
                ['discard if not={{{field}}}{{{value}}}'.format(field=f, value=group[f]) for f in self.group_by if
                 f is not None and group[f] is not None])

            group['data'] = csv.where(group)

            legend = None
            if callable(self.legend):
                legend = self.legend(group)
            if isinstance(self.legend, (tuple, list)):
                legend = self.legend[leg_count]
                leg_count += 1

            options = [discard]
            if callable(self.plot_opts):
                options = options + self.plot_opts(group)

            index = None
            if callable(self.style_index):
                index = self.style_index(group)

            tex.add_plot(
                x=self.x, y=self.y,
                table=tables[0],
                options=options,
                legend=legend,
                style=index,
            )

        # Linear lines
        # TODO use fitting
        groups = csv.get_groups(self.group_by)

        for group in groups:
            group_data = csv.where(group)
            group_ranks = list(map(int, group_data['n_ranks']))
            row = find_rows(group_data, {'n_ranks': min(group_ranks)})
            values = list(map(float, [group_data[self.y][i] for i in row]))
            m = max(values) * min(group_ranks)
            tex.add_plot(
                y='{m} / x'.format(m=m),
                options=['no marks', 'dotted', 'thick', 'black',
                         'domain={}:{}'.format(min(group_ranks), max(group_ranks)),
                         ],
            )

        return tex


class Speedup:
    def __init__(self, name='', source=None, fieldname=None, axis_opts=None, plot_opts=None, group_by=None):
        self.name = name
        self.source = source
        self.x = 'n_ranks'
        self.y = fieldname
        self.group_by = group_by or 'run'

        self.axis_opts = []
        self.axis_opts.append(r'xlabel={\# cores}')
        self.axis_opts.append('ylabel={Speedup}')
        self.axis_opts.append('xtick=data, xmode=log, log basis x={2}')
        self.axis_opts.append('ymode=log, log basis y={2}')
        self.axis_opts.append('log ticks with fixed point')
        self.axis_opts.append('legend pos = north west')
        self.axis_opts.append('grid=both')
        self.axis_opts.append('grid style={dotted, gray}')

        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)

        self.plot_opts = []
        if plot_opts is not None:
            self.plot_opts.extend(plot_opts)

    def generate(self):
        assert self.y is not None

        normalize_one = False

        if isinstance(self.source, CSV):
            csvs = [self.source]
            tables = [self.source.filename]
        else:
            csvs = make_list(self.source.get_summary())
            tables = self.source.get_files()

        ranks = set([r for csv in csvs for r in map(int, csv['n_ranks'])])
        groups = list(set([g for csv in csvs for g in csv[self.group_by]]))
        groups.sort()

        tex = configuration.Default.new_tex()
        tex.set_axis_opts(
            self.axis_opts + ['xtick={{{ticks}}}'.format(ticks=','.join([str(r) for r in sorted(ranks)]))])

        for csv, table in zip(csvs, tables):
            tex.add_tables(table)

            for i in groups:
                tmp = csv.where({self.group_by: lambda v: int(v) == i})
                run_ranks = list(map(int, tmp['n_ranks']))
                row = find_rows(csv, {self.group_by: i, 'n_ranks': min(run_ranks)})
                assert len(row) == 1
                row = row[0]
                lvl = csv['lvl'][row]
                scaling = float(csv[self.y][row])

                if not normalize_one:
                    scaling = scaling * min(ranks)

                tex.add_plot(
                    x=self.x, y=r'expr={s} / \thisrow{{{y}}}'.format(s=scaling, y=self.y),
                    table=table,
                    options=self.plot_opts + [
                        'discard if not={{{field}}}{{{value}}}'.format(field=self.group_by, value=i)],
                    legend=r'$\ell = {}$'.format(lvl)
                )

                # Linear lines
                if i == groups[-1]:
                    tex.add_plot(
                        y='x / {m}'.format(m=min(ranks) if normalize_one else 1),
                        options=['no marks', 'dotted', 'thick', 'black',
                                 'domain={}:{}'.format(min(run_ranks), max(run_ranks)),
                                 ],
                    )

        return tex


class GroupPlot:
    def __init__(self, name='',
                 plots=None,
                 axis_opts=None, plot_opts=None,
                 improve_labels=False,
                 improve_axis=False,
                 unify_bounds=False,
                 plots_with_legend=None):
        self.name = name
        self.plots = plots
        self.improve_labels = improve_labels
        self.improve_axis = improve_axis
        self.unify_bounds = unify_bounds

        self.axis_opts = []
        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)
        if len(self.axis_opts) == 0:
            self.axis_opts.append('group style={{group size={} by {}, {} }}'.format(
                len(plots),
                1,
                configuration.Default.group_style)
            )

        self.plot_opts = []
        if plot_opts is not None:
            self.plot_opts.extend(plot_opts)

        self.plots_with_legend = plots_with_legend or list(range(len(plots)))

    def __str__(self):
        return 'GroupPlot[{}]'.format(', '.join([str(P) for P in self.plots]))

        # def positions(i, n_rows, n_cols):
        #     n = n_rows * n_cols
        #     on_top = i < n_cols
        #     on_bottom = i >= n - n_cols
        #     on_left = i % n_cols == 0
        #     on_right = i % n_cols == n_cols - 1
        #
        #     result = []
        #
        #     if on_left:
        #         result.append('ytick pos=left, yticklabel pos=left')
        #     if on_right:
        #         result.append('ytick pos=right, yticklabel pos=right')
        #     if on_top:
        #         result.append('xticklabel pos=top')
        #     if on_bottom:
        #         result.append('xticklabel pos=bottom')
        #
        #     return result

    # Return (n_cols, n_rows)
    def get_group_size(self):
        for s in self.axis_opts:
            # group size = 2 by 1
            matches = re.findall(r'groupsize=(\d*)by(\d*)', s.replace(' ', ''))
            if len(matches) > 0:
                matches = matches[0]
                assert len(matches) == 2
                return tuple(map(int, matches))

    def get_label_pos(self, i):
        size = self.get_group_size()
        if (i % size[0]) == 0:
            return None
        return None

    def generate(self):
        assert self.plots is not None

        tex = configuration.Default.new_tex()
        tex.axis_opts = []  # TODO no axis opts for group plot
        for i, P in enumerate(self.plots):
            if self.improve_labels:
                size = self.get_group_size()
                n_rows = size[1]
                n_cols = size[0]

                axis_improvements = []
                col = i % n_cols
                row = i // n_cols
                # Move y-label to right if plot is last in row
                if col + 1 == n_cols:
                    axis_improvements.append('yticklabel pos = right')
                # Remove x label if plot is not in bottom row
                if 1 < n_rows != row + 1:
                    axis_improvements.append('xlabel = {}')

                # axis_improvements = [a for a in axis_improvements if a is not None]
                P.axis_opts.extend(axis_improvements)

            if i not in self.plots_with_legend:
                P.legend = None

            tex_P = P.generate()
            tex.add_group(tex_P)

            # TODO figure out min/max y-values
            # if self.unify_bounds:
            #     y_values = []
            #     for p in tex_P.plots:
            #         # Search expression
            #         r = re.findall(pattern=r'table\[.+yexpr=(.+)\]', string=p.replace(' ', ''))
            #         for expr in r:
            #             print(get_values(expr, data))

        tex.set_axis_opts(self.axis_opts)

        return tex


class LevelPlot(NewPlot):
    def __init__(self, name, source, x, y, axis_opts=None, plot_opts=None):
        NewPlot.__init__(self,
                         name=name,
                         source=source,
                         x=x,
                         y=y,
                         axis_opts=axis_opts,
                         plot_opts=plot_opts
                         )

    def add_plot(self, y, table):
        self.tex.add_plot(
            x=self.x, y=y,
            table=table,
            options=self.plot_opts,
            legend=r'$\ell = \LVL{{{table}}}$'.format(table=table)
        )


class Iterations(MultiPlot):
    def __init__(self, **kwargs):
        MultiPlot.__init__(self, **kwargs)
        self.x = ['time']
        self.y = [r'expr=\thisrow{n_lin_iter} / \thisrow{n_active_set_iter}']
        self.axis_opts.extend([
            r'xlabel={Time}',
            r'ylabel={\# Iterations / Active-Set}',
            r'ymin=0',
        ])

    def __str__(self):
        return 'Iterations[{}]'.format(self.name)


class NVmult(MultiPlot):
    def __init__(self, dim, **kwargs):
        MultiPlot.__init__(self, **kwargs)
        self.dim = dim

        def __str__(self):
            return 'NVmult[{}]'.format(self.name)

        def add_plots(self, tables):
            for table in tables:
                cols = read_csv(table).keys()
                cols = [c for c in cols if any(matches(c, r'vmult'))]
                cols.sort()

                max_lvl = int(read_csv(table)['lvl'][0])

                y = r'expr = \thisrow{vmult_fine} * 1 * 1'
                # vmult_fine vmult_lvl_2 vmult_lvl_1_00
                for c in cols:
                    m = re.search(r'vmult_lvl_(?P<lvl>\d*)(_(?P<block>[01]{2}))?', c)
                    if m is not None:
                        lvl = int(m.group('lvl'))
                        block = m.group('block')

                        f_block = None
                        if block is None:
                            f_block = '1'
                        if block == '11':
                            f_block = '1/({dim}+1)'.format(dim=self.dim)
                        if block == '00':
                            f_block = '{dim}/({dim}+1)'.format(dim=self.dim)
                        assert f_block

                        f_lvl = '1/(2^{dim})^{exp}'.format(dim=self.dim, exp=max_lvl - lvl - 1)

                        y += r' + \thisrow{{{c}}} * {fb} * {fl}'.format(c=c, fb=f_block, fl=f_lvl)

                self.tex.add_plot(
                    x=self.x, y=y,
                    table=table,
                    options=self.plot_opts,
                    legend=r'$\ell = \LVL{{{table}}}$'.format(table=table)
                )


# class NVmult(NewPlot):
#         def __init__(self, name, source, dim, axis_opts=None, plot_opts=None):
#             NewPlot.__init__(self,
#                              name=name,
#                              source=source,
#                              axis_opts=axis_opts,
#                              plot_opts=plot_opts,
#                              x='time', y='time',
#                              )
#             self.dim = dim
#
#         def __str__(self):
#             return 'NVmult[{}]'.format(self.name)
#
#         def add_plots(self, tables):
#             for table in tables:
#                 cols = read_csv(table).keys()
#                 cols = [c for c in cols if any(matches(c, r'vmult'))]
#                 cols.sort()
#
#                 max_lvl = int(read_csv(table)['lvl'][0])
#
#                 y = r'expr = \thisrow{vmult_fine} * 1 * 1'
#                 # vmult_fine vmult_lvl_2 vmult_lvl_1_00
#                 for c in cols:
#                     m = re.search(r'vmult_lvl_(?P<lvl>\d*)(_(?P<block>[01]{2}))?', c)
#                     if m is not None:
#                         lvl = int(m.group('lvl'))
#                         block = m.group('block')
#
#                         f_block = None
#                         if block is None: f_block = '1'
#                         if block == '11': f_block = '1/({dim}+1)'.format(dim=self.dim)
#                         if block == '00': f_block = '{dim}/({dim}+1)'.format(dim=self.dim)
#                         assert f_block
#
#                         f_lvl = '1/(2^{dim})^{exp}'.format(dim=self.dim, exp=max_lvl-lvl-1)
#
#                         y += r' + \thisrow{{{c}}} * {fb} * {fl}'.format(c=c, fb=f_block, fl=f_lvl)
#
#                 self.tex.add_plot(
#                     x=self.x, y=y,
#                     table=table,
#                     options=self.plot_opts,
#                     legend=r'$\ell = \LVL{{{table}}}$'.format(table=table)
#                 )


class Displacement(LevelPlot):
    def __init__(self, name, source, axis_opts=None):
        LevelPlot.__init__(self,
                           name=name,
                           source=source,
                           x='time',
                           y='disp_u',
                           axis_opts=['xlabel={Time}', 'ylabel={Displacement $u [mm]$}'] + (
                               axis_opts if axis_opts is not None else [])
                           )


class ActiveSet(LevelPlot):
    def __init__(self, name, source, axis_opts=None):
        LevelPlot.__init__(self,
                           name=name,
                           source=source,
                           x='time',
                           y='n_active_set_iter',
                           axis_opts=['xlabel={Time}', 'ylabel={\\# Active-Set}', 'ymin=0'] + (
                               axis_opts if axis_opts is not None else [])
                           )

    def __str__(self):
        return 'ActiveSet[{}]'.format(self.name)


class CrackEnergy(LevelPlot):
    def __init__(self, name, source, axis_opts=None):
        LevelPlot.__init__(self,
                           name=name,
                           source=source,
                           x='time',
                           y='crack_energy',
                           axis_opts=['xlabel={Time}', 'ylabel={Energy}'] + (axis_opts if axis_opts is not None else [])
                           )

    def __str__(self):
        return 'CrackEnergy[{}]'.format(self.name)


class BulkEnergy(LevelPlot):
    def __init__(self, name, source, axis_opts=None):
        LevelPlot.__init__(self,
                           name=name,
                           source=source,
                           x='time',
                           y='bulk_energy',
                           axis_opts=['xlabel={Time}', 'ylabel={Energy}'] + (axis_opts if axis_opts is not None else [])
                           )

    def __str__(self):
        return 'BulkEnergy[{}]'.format(self.name)


class LoadDisplacement(MultiPlot):
    def __init__(self, boundary=None, component=None, negative=False, **kwargs):
        MultiPlot.__init__(self, **kwargs)

        self.boundary = boundary
        self.component = component
        self.negative = negative

        assert self.source
        assert isinstance(self.source, Tables)

    def __str__(self):
        return 'LoadDisplacement[{}]'.format(self.name)

    def generate(self, **kwargs):
        have_full_load = False
        for c in self.source:
            for k in c.keys():
                if 'load_full' in k:
                    have_full_load = True
            break

        sign = '-' if self.negative else ''

        # == g(pf)*stress + (stress - stress^+) == g(pf)*stress + stress^-
        expr = r' expr={sign}(' \
               r'\thisrow{{load_split_pf_{boundary}_{component}}} ' \
               r'+ \thisrow{{load_{boundary}_{component}}} ' \
               r'- \thisrow{{load_split_{boundary}_{component}}}' \
               r')'.format(sign=sign, boundary=self.boundary, component=self.component)
        # expr = r'expr={sign}(\thisrow{{load_{boundary}_{component}}})'.format(
        #     sign=sign, boundary=self.boundary, component=self.component)
        field = r'expr={sign}\thisrow{{load_full_{boundary}_{component}}}'.format(
            sign=sign, boundary=self.boundary, component=self.component)

        self.x = ['disp_u']
        self.y = [field if have_full_load else expr]
        self.axis_opts.extend([
            'xlabel={Displacement $u$}',
            'ylabel={Load $F_y [kN]$}',
        ])

        return MultiPlot.generate(self, **kwargs)


class COD(MultiPlot):
    def __init__(self, use_pf=True, postfix='', **kwargs):
        self.use_pf = use_pf
        self.postfix = postfix

        axis_opts = [
            'legend pos = north west',
            'no markers',
            'grid=both',
            'grid style={dotted, gray}',
            r'xlabel=$x$',
            r'ylabel=$COD(x)$',
        ]

        if 'axis_opts' in kwargs:
            axis_opts.extend(kwargs.pop('axis_opts'))

        y = ''
        if not self.use_pf:
            y = 'cod_eps'
        else:
            # if TW:
            y = r'expr = \thisrow{cod_pf}'
            # else:
            # y = r'expr = 10*\thisrow{cod_pf} * \thisrow{h}/sqrt(2.0)'

        MultiPlot.__init__(
            self,
            **kwargs,
            axis_opts=axis_opts,
            x='x',
            y=y,
        )

    def generate(self, base_path='', tex=None):
        exact_file = self.source.tables()[-1]['@cod_file'][-1]
        exact_data = CSV(filename=exact_file)
        exact_cod = MultiPlot(
            x='x',
            y=r'expr=\thisrow{cod_exact}' if self.use_pf else 'cod_eps_exact',
            legend='COD' if self.use_pf else r'$\mbox{COD}_y$',
            plot_opts=[
                # 'densely dotted',
                'black',
            ],
            source=exact_data,
        )

        tex = MultiPlot.generate(self, base_path, tex)
        tex.tables = [res.replace('results_', 'cod{}_'.format(self.postfix)) for res in
                      tex.tables]  # TODO thats a bit whacky
        tex.plots = [res.replace('results_', 'cod{}_'.format(self.postfix)) for res in
                     tex.plots]  # TODO thats a bit whacky
        tex.add(exact_cod.generate(base_path))
        return tex


# Container for Roofline elements
class Kernel:
    def __init__(self,
                 name: str = None,
                 fps: float = 0,
                 ai: float = 0,
                 options=None,
                 ):
        self.name = name
        self.flops_per_second = fps
        self.flops_per_byte = ai
        self.options = options


class Roofline:
    def __init__(self, name=None,
                 bandwidths: dict = None,
                 flops: dict = None,
                 kernels: list = None,

                 highlight: list = None,

                 options=None, options_highlight=None,
                 xmin=None, xmax=None,
                 ymin=None, ymax=None,

                 axis_opts: list = None,
                 angle=0,
                 ):
        self.name = name

        self.bandwidths = bandwidths or dict()
        self.flops = flops or dict()
        self.kernels = kernels or []

        self.highlight = highlight or []

        self.options = options or ['black', 'dashed']
        self.options_highlight = options_highlight or ['black', 'thick']

        self.angle = angle

        self.x_min = xmin or 1.0e-4
        self.x_max = xmax or 2 * max([k.flops_per_byte for k in kernels])

        self.y_min = ymin or 1.0e-3
        self.y_max = ymax or 5 * max(self.flops.values())

        self.axis_opts = [
            'xlabel={Flops / Byte}',
            'ylabel={Flops / Second}',
            'no markers',
            # 'log x ticks with fixed point',
            'xmode = log', 'log basis x = {2}',
            'ymode = log',
            'domain={{{min}:{max}}}'.format(min=self.x_min, max=self.x_max),
            'enlargelimits=false',
            'axis x line*=bottom',
            # 'axis y line*=left',
            'xmin={}'.format(self.x_min),
            'xmax={}'.format(self.x_max),
            'ymin={}'.format(self.y_min),
            'ymax={}'.format(self.y_max),
        ]

        self.axis_opts.append('grid=both')
        self.axis_opts.append('grid style={dotted, lightgray}')

        if axis_opts is not None:
            self.axis_opts.extend(axis_opts)

    def add_bands(self, tex):
        for name, bandwidth in self.bandwidths.items():
            i = max(self.flops.values()) / bandwidth
            tex.add_parametric_plot(
                append=False,
                x='x', y='{}*x'.format(bandwidth),
                options=[
                            'domain={}:{}'.format(self.x_min, i)
                        ] + (self.options_highlight if name in self.highlight else self.options)
            )
            if name in self.highlight:
                angle = 0 or self.angle

                tex.plots.append(
                    r'\node[label={{[label distance=0.2cm,text depth=2ex,rotate={angle}]right:\tiny {name} }}] () at ( {x}, {y} ) {{}};'.format(
                        name=name,
                        angle=angle,
                        x=self.x_min,
                        y=bandwidth * self.x_min,
                    )
                )

    def add_flops(self, tex):
        for name, flops in self.flops.items():
            if len(self.bandwidths.values()) > 0:
                i = flops / max(self.bandwidths.values())
            else:
                i = self.x_min

            tex.add_parametric_plot(
                append=False,
                x='x', y='{}'.format(flops),
                options=[
                            'domain={}:{}'.format(i, self.x_max),
                        ] + (self.options_highlight if name in self.highlight else self.options),
            )
            if name in self.highlight:
                tex.plots.append(
                    r'\node[above, label=left:{{\tiny {name} }}] () at ( {x}, {y} ) {{  }};'.format(name=name,
                                                                                                    x=self.x_max,
                                                                                                    y=flops))

    def generate(self, base_path=''):
        tex = configuration.Default.new_tex()
        tex.set_axis_opts(self.axis_opts)

        # x = F/B
        # y = F/s
        #  F/s = y
        # GB/s = (F/s) / (F/B) = y/x

        self.add_flops(tex)
        self.add_bands(tex)

        for k in self.kernels:
            assert isinstance(k, Kernel)

            opts = ''
            if k.options:
                if isinstance(k.options, list):
                    opts = ', '.join(k.options)
                else:
                    if isinstance(k.options, str):
                        opts = k.options
                    else:
                        raise Exception('Invalid option format.')

            tex.plots.append(
                r'\node[fill, label={name}, scale=0.3, {opts}] () at ( {x}, {y} ) {{}};'.format(
                    name=k.name,
                    x=k.flops_per_byte,
                    y=k.flops_per_second,
                    opts=opts,
                )
            )

        return tex


try:
    from scipy.spatial import ConvexHull, convex_hull_plot_2d
    from ellipse import *

    _scipy_available_ = True
except ImportError:
    print('SciPy not available')
    _scipy_available_ = False

if _scipy_available_:
    class Ellipse(MultiPlot):
        def __init__(self, **kwargs):
            MultiPlot.__init__(self, **kwargs)

            assert isinstance(self.source, CSV)

            # self.x = ['time']
            # self.y = [r'expr=\thisrow{n_lin_iter} / \thisrow{n_active_set_iter}']
            # self.axis_opts.extend([
            #     'xlabel={Timestep}', 'ylabel={\# Iterations / Active Set}',
            #     'ymin=0',
            # ])

        def __str__(self):
            return 'Ellipse[{}]'.format(self.name)

        def generate(self, base_path=''):
            tex = configuration.Default.new_tex()
            tex.set_axis_opts(self.axis_opts)
            tex.add_tables(self.source.filename)

            # Plot points
            tex.add_plot(table=self.source.filename, options=['only marks', 'mark=x'])

            # Plot convex hull
            x = self.source['x']
            y = self.source['y']
            points = list(zip(x, y))
            hull = ConvexHull(points)
            coordinates = ' '.join(['{}'.format(points[i]) for i in hull.vertices])
            coordinates += '{}'.format(points[0])
            tex.add_plot(raw='\\addplot[mark=x] coordinates {{ {xy}  }};'.format(xy=coordinates))

            # Plot ellipse
            x = [points[i][0] for i in hull.vertices]
            y = [points[i][1] for i in hull.vertices]

            params = fit_ellipse(np.array(x), np.array(y))
            # a - major axis length
            # b - minor axis length
            # cx - ellipse centre (x coord.)
            # cy - ellipse centre (y coord.)
            # phi - rotation angle of ellipse bounding box
            a, b, cx, cy, phi = params
            assert abs(cy) <= 1.0e-15
            tex.add_plot(
                raw='\\addplot[domain=-pi:pi, samples=200] '
                    '( {{ {cx} + {a}*cos(deg(x)) }}, {{ {cy} + {b}*sin(deg(x)) }} );'
                    .format(cx=cx, cy=cy, a=a, b=b, phi=phi))

            return tex
